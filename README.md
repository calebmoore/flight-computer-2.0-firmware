# Flight Computer Firmware

This repository contains the flight and testing code for the ESS 472 Flight Computer (TLM v5.2, v5.3). 

## Getting Started

The full design document for the firmware is found in the [wiki][wiki], which serves as a guide for setting up the development environment, programming the computer itself, and as an overall view of the source directory and its major components.

Detailed documentation of code functionality is found in the source code itself.

[wiki]: https://bitbucket.org/calebmoore/flight-computer-2.0-firmware/wiki "Wiki"


