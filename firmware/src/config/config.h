/*
 * config.h
 *
 * Created: 1/31/2015 11:40:32 AM
 *  Author: Caleb
 */ 


#ifndef CONFIG_H_
#define CONFIG_H_

#include <asf.h>

// RTOS task priorities
#define TASK_PRIORITY_PLATFORM     (tskIDLE_PRIORITY + 2)
#define TASK_PRIORITY_TLM_TX       (tskIDLE_PRIORITY + 2)
#define TASK_PRIORITY_TLM_RX       (tskIDLE_PRIORITY + 1) 
#define TASK_PRIORITY_GPS          (tskIDLE_PRIORITY + 2)
#define TASK_PRIORITY_RADIO_LINK   (tskIDLE_PRIORITY + 3)
#define TASK_PRIORITY_LOG          (tskIDLE_PRIORITY + 1)
#define TASK_PRIORITY_MEM          (tskIDLE_PRIORITY + 1)
#define TASK_PRIORITY_SENSORS      (tskIDLE_PRIORITY + 4)
#define TASK_PRIORITY_ADC
#define TASK_PRIORITY_EXP

// RTOS task names
#define TASK_NAME_PLATFORM         "PLATFORM"
#define TASK_NAME_LOG              "LOGGING"
#define TASK_NAME_MEM              "MEMORY"
#define TASK_NAME_SENSORS          "SENSORS"
#define TASK_NAME_TLM_TX           "TLM TX"
#define TASK_NAME_TLM_RX           "TLM RX"
#define TASK_NAME_GPS              "GPS"
#define TASK_NAME_RADIO_LINK       "RADIOLNK"

// RTOS task sizes
#define TASK_STACK_SIZE_PLATFORM   (2 * configMINIMAL_STACK_SIZE)
#define TASK_STACK_SIZE_TLM_TX     configMINIMAL_STACK_SIZE
#define TASK_STACK_SIZE_TLM_RX     configMINIMAL_STACK_SIZE
#define TASK_STACK_SIZE_LOG        configMINIMAL_STACK_SIZE
#define TASK_STACK_SIZE_MEM        configMINIMAL_STACK_SIZE
#define TASK_STACK_SIZE_SENSORS    (3 * configMINIMAL_STACK_SIZE)
#define TASK_STACK_SIZE_GPS        configMINIMAL_STACK_SIZE
#define TASK_STACK_SIZE_RADIO_LINK configMINIMAL_STACK_SIZE

// Pyro configuration
#define PYRO_DURATION 1000 // time in ms to burn each pyro

/**
 * GPS modules sold in the US must are not permitted to operate faster than
 * 1000 kts at an altitude over 60,000 ft under ITAR restrictions
 * (ITAR Category XV.c.2). 
 *
 * The GPS module is preemptively disabled if either of these limits is 
 * exceeded.
 */
#define ITAR_LIMIT_ALT   18000.0 // altitude in m (59055 ft)
#define ITAR_LIMIT_SPEED 500.0   // speed in m/s (972 kts)

/**
 * Flight Thresholds 
 */
#define THRESHOLD_LIFTOFF_ACCEL 5.0 // gees

#endif /* CONFIG_H_ */