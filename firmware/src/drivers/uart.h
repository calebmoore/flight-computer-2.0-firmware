/*
 * uart.h
 *
 * Created: 2/4/2015 5:44:38 PM
 *  Author: Winglee Plasma Lab
 */ 


#ifndef UART_H_
#define UART_H_

#include <asf.h>

#include "debug.h"


#ifdef DEBUG_OVER_USART
#   define UART_BAUDRATE        2000000
#else
#   define UART_BAUDRATE        9600
#endif

#define UART_SERCOM             SERCOM2
#define UART_SERCOM_MODE        USART_RX_1_TX_0_XCK_1
#define UART_SERCOM_PAD0_PINMUX PINMUX_PA12C_SERCOM2_PAD0
#define UART_SERCOM_PAD1_PINMUX PINMUX_PA13C_SERCOM2_PAD1
#define UART_SERCOM_PAD2_PINMUX PINMUX_UNUSED
#define UART_SERCOM_PAD3_PINMUX PINMUX_UNUSED


void uart_init(void);
bool uart_test(void);

int uart_putchar(int ch);

#endif /* UART_H_ */