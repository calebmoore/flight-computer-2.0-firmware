/*
 * pyros.h
 *
 * Created: 2/4/2015 8:00:07 PM
 *  Author: Winglee Plasma Lab
 */ 


#ifndef PYROS_H_
#define PYROS_H_

#include <asf.h>

#define ALLOW_EXTERNAL_PYRO_FIRING

#define PYRO_1_FIRE_PIN            PIN_PA04
#define PYRO_2_FIRE_PIN            PIN_PA05
#define PYRO_3_FIRE_PIN            PIN_PA06
#define PYRO_4_FIRE_PIN            PIN_PA07

#ifdef ALLOW_EXTERNAL_PYRO_FIRING
#define PYRO_1_EXT_FIRE_PIN        PIN_PB00
#define PYRO_2_EXT_FIRE_PIN        PIN_PB01
#define PYRO_3_EXT_FIRE_PIN        PIN_PB02
#define PYRO_4_EXT_FIRE_PIN        PIN_PB03

#define PYRO_1_EXT_FIRE_EIC_PIN    PIN_PB00A_EIC_EXTINT0
#define PYRO_2_EXT_FIRE_EIC_PIN    PIN_PB01A_EIC_EXTINT1
#define PYRO_3_EXT_FIRE_EIC_PIN    PIN_PB02A_EIC_EXTINT2
#define PYRO_4_EXT_FIRE_EIC_PIN    PIN_PB03A_EIC_EXTINT3

#define PYRO_1_EXT_FIRE_EIC_MUX    MUX_PB00A_EIC_EXTINT0
#define PYRO_2_EXT_FIRE_EIC_MUX    MUX_PB01A_EIC_EXTINT1
#define PYRO_3_EXT_FIRE_EIC_MUX    MUX_PB02A_EIC_EXTINT2
#define PYRO_4_EXT_FIRE_EIC_MUX    MUX_PB03A_EIC_EXTINT3

#define PYRO_1_EXT_FIRE_EIC_LINE   (0)
#define PYRO_2_EXT_FIRE_EIC_LINE   (1)
#define PYRO_3_EXT_FIRE_EIC_LINE   (2)
#define PYRO_4_EXT_FIRE_EIC_LINE   (3)
#endif

typedef enum {
    PYRO_1,
    PYRO_2,
    PYRO_3,
    PYRO_4,
    PYRO_NONE
} Pyro;

#define PYRO_AUX       PYRO_4
#define PYRO_2ND_STAGE PYRO_3
#define PYRO_MAIN      PYRO_2
#define PYRO_DROGUE    PYRO_1

void pyros_init(void);
bool pyros_test(void);

void pyros_arm(void);
void pyros_disarm(void);
bool pyros_is_armed(void);

void pyros_fire_pyro(Pyro pyro);
void pyros_fire_pyro_wait(Pyro pyro, uint16_t delay);

#endif /* PYROS_H_ */