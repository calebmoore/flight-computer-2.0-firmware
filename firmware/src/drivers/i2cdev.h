/**
 * 
 */

#ifndef _I2CDEV_H_
#define _I2CDEV_H_

#include <asf.h>

/**
 * Initialize the I2C device instance.
 */
bool i2cdev_init(void);

/**
 * Read multiple bytes from the I2C device at the given address.
 *
 * @param device  pointer to I2C master module
 * @param address 7-bit address of device
 * @param reg     register in device memory to read from
 * @param length  how many bytes to read
 * @param data    pointer to byte array holding received data
 * @return status code 
 */
enum status_code i2cdev_read(struct i2c_master_module *device, uint8_t address, 
        uint8_t reg, uint16_t length, uint8_t *data);

/**
 * Read multiple bytes from the 16-bit I2C device at the given address with.
 *
 * @param device  pointer to I2C master module
 * @param address 7-bit address of device
 * @param reg     16-bit register in device memory to read from
 * @param length  how many bytes to read
 * @param data    pointer to byte array holding received data
 * @return status code 
 */        
//enum status_code i2cdev_read_16(struct i2c_master_module *device, uint8_t address,
        //uint16_t reg, uint16_t length, uint8_t *data);

/**
 * Read a single byte from the I2C device at the given address.
 *
 * @param device  pointer to I2C master module
 * @param address 7-bit address of device
 * @param reg     register in device memory to read from
 * @param data    pointer to byte holding received data
 * @return status code 
 */
enum status_code i2cdev_read_byte(struct i2c_master_module *device, uint8_t address,
        uint8_t reg, uint8_t *data);

/**
 * Read multiple bits from the I2C device at the given address.
 *
 * @param device  pointer to I2C master module
 * @param address 7-bit address of device
 * @param reg     register in device memory to read from
 * @param bit_num number of bit to read
 * @param data    pointer to byte holding received data
 * @return status code 
 */
enum status_code i2cdev_read_bit(struct i2c_master_module *device, uint8_t address,
        uint8_t reg, uint8_t bit_num, uint8_t *data);

/**
 * Read multiple bits from the I2C device at the given address.
 *
 * @param device    pointer to I2C master module
 * @param address   7-bit address of device
 * @param reg       register in device memory to read from
 * @param bit_start first bit to read
 * @param length    how many bits to read
 * @param data      pointer to byte holding received data
 * @return status code 
 */
enum status_code i2cdev_read_bits(struct i2c_master_module *device, uint8_t address,
        uint8_t reg, uint8_t bit_start, uint8_t length, uint8_t *data);

/**
 * Write multiple bytes to the I2C device at the given address.
 *
 * @param device    pointer to I2C master module
 * @param address   7-bit address of device
 * @param reg       register in device memory to write to
 * @param length    how many bytes to write
 * @param data      pointer to byte holding data
 * @return status code 
 */
enum status_code i2cdev_write(struct i2c_master_module *device, uint8_t address,
        uint8_t reg, uint16_t length, uint8_t *data);

/**
 * Write multiple bytes to the 16-bit I2C device at the given address.
 *
 * @param device    pointer to I2C master module
 * @param address   7-bit address of device
 * @param reg       16-bit register in device memory to write to
 * @param length    how many bytes to write
 * @param data      pointer to byte holding data
 * @return status code 
 */
//enum status_code i2cdev_write_16(struct i2c_master_module *device, uint8_t address,
        //uint16_t reg, uint16_t length, uint8_t *data);

/**
 * Write a byte to the I2C device at the given address.
 *
 * @param device    pointer to I2C master module
 * @param address   7-bit address of device
 * @param reg       register in device memory to write to
 * @param data      byte to write
 * @return status code 
 */
enum status_code i2cdev_write_byte(struct i2c_master_module *device, uint8_t address, 
        uint8_t reg, uint8_t data);

/**
 * Write a bit to the I2C device at the given address.
 *
 * @param device    pointer to I2C master module
 * @param address   7-bit address of device
 * @param reg       register in device memory to write to
 * @param bit_num   number of bit to write
 * @param data      data to write
 * @return status code 
 */
enum status_code i2cdev_write_bit(struct i2c_master_module *device, uint8_t address,
        uint8_t reg, uint8_t bit_num, uint8_t data);

/**
 * Write multiple bits to the I2C device at the given address.
 *
 * @param device    pointer to I2C master module
 * @param address   7-bit address of device
 * @param reg       register in device memory to write to
 * @param bitStart  the bit to start from
 * @param length    how many bits to write
 * @param data      data to write
 * @return status code 
 */
enum status_code i2cdev_write_bits(struct i2c_master_module *device, uint8_t address,
        uint8_t reg, uint8_t bit_start, uint8_t length, uint8_t data);

#endif /* _I2CDEV_H_ */