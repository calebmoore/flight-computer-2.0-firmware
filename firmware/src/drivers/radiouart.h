/*
 * radiouart.h
 *
 * Created: 2/20/2015 5:49:11 PM
 *  Author: Caleb
 */ 


#ifndef RADIO_UART_H_
#define RADIO_UART_H_

#include <asf.h>

#include "eprintf.h"

#define RADIO_UART_BAUDRATE    9600
#define RADIO_UART_SERCOM      SERCOM5
#define RADIO_UART_SERCOM_MODE USART_RX_1_TX_0_XCK_1
#define RADIO_UART_PINMUX_PAD0 PINMUX_PB16C_SERCOM5_PAD0
#define RADIO_UART_PINMUX_PAD1 PINMUX_PB17C_SERCOM5_PAD1
#define RADIO_UART_PINMUX_PAD2 PINMUX_UNUSED
#define RADIO_UART_PINMUX_PAD3 PINMUX_UNUSED

#define radio_uart_printf(FMT, ...) eprintf(radio_uart_putchar, FMT, ## __VA_ARGS__)

void radio_uart_init(void);
bool radio_uart_test(void);

bool radio_uart_get_data_with_timeout(uint8_t *c);

void radio_uart_send_data_wait(uint16_t len, uint8_t *c);

void radio_uart_send_data_isr_blocking(uint16_t len, uint8_t *c);

int radio_uart_putchar(int ch);


#endif /* RADIO_UART_H_ */