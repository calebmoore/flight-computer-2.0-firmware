/*
 * i2cdev.c
 *
 * Created: 1/30/2015 10:52:42 AM
 *  Author: Winglee Plasma Lab
 */ 

#include <asf.h>
#include "i2cdev.h"

#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"

#include "statind.h"

#define I2C_DEV_DEFAULT_TIMEOUT (1000)

static struct dma_resource i2c_dma_resource_tx;

static volatile bool transfer_is_done = false;

COMPILER_ALIGNED(16)
DmacDescriptor i2c_descriptor;

xSemaphoreHandle i2cdev_dma_event;

static void configure_dma_resource(struct dma_resource *resource, uint8_t dma_trigger);
static void setup_dma_descriptor(DmacDescriptor *descriptor, 
        struct i2c_master_module *i2c_device, uint16_t length, uint8_t *data);
static void transfer_done(const struct dma_resource *const resource);

/**
 * 
 */
bool i2cdev_init() {
    
    configure_dma_resource(&i2c_dma_resource_tx, SERCOM4_DMAC_ID_TX);
    
    dma_register_callback(&i2c_dma_resource_tx, transfer_done, DMA_CALLBACK_TRANSFER_DONE);
    dma_enable_callback(&i2c_dma_resource_tx, DMA_CALLBACK_TRANSFER_DONE);
    
    vSemaphoreCreateBinary(i2cdev_dma_event);
    
    return true;    
}

/**
 * 
 */
enum status_code i2cdev_read_byte(struct i2c_master_module *device, 
        uint8_t address, uint8_t reg, uint8_t *data) 
{
    return i2cdev_read(device, address, reg, 1, data);        
}

/**
 * 
 */
enum status_code i2cdev_read_bit(struct i2c_master_module *device, 
        uint8_t address, uint8_t reg, uint8_t bit_num, uint8_t *data)
{
    uint8_t byte;
    enum status_code status;
    
    status = i2cdev_read(device, address, reg, 1, &byte);
    *data = byte & (1 << bit_num);
    
    return status;
}

/**
 * 
 */
enum status_code i2cdev_read_bits(struct i2c_master_module *device, 
        uint8_t address, uint8_t reg, uint8_t bit_start, uint8_t length, 
        uint8_t *data)
{
    uint8_t byte;
    enum status_code status;
    
    if ((status = i2cdev_read_byte(device, address, reg, &byte)) == STATUS_OK) {
        uint8_t mask = ((1 << length) - 1) << (bit_start - length + 1);
        byte &= mask;
        byte >>= (bit_start - length + 1);
        *data = byte;
    }
    
    return status;
}

/**
 * 
 */
enum status_code i2cdev_read(struct i2c_master_module *device, 
        uint8_t address, uint8_t reg, uint16_t length, uint8_t *data)
{
    //struct i2c_master_packet packet = {
        //.address = address,
        //.data_length = 1,
        //.data = &reg,
        //.ten_bit_address = false,
        //.high_speed = false,
        //.hs_master_code = 0x0
    //};
    
    xSemaphoreTake(i2cdev_dma_event, portMAX_DELAY);
    
    setup_dma_descriptor(&i2c_descriptor, device, 1, &reg);
    dma_add_descriptor(&i2c_dma_resource_tx, &i2c_descriptor);
    
    dma_start_transfer_job(&i2c_dma_resource_tx);
    i2c_master_dma_set_transfer(device, address, length, I2C_TRANSFER_WRITE);
    
    while (!transfer_is_done);
    
    xSemaphoreTake(i2cdev_dma_event, portMAX_DELAY);
    
    setup_dma_descriptor(&i2c_descriptor, device, length, data);
    dma_add_descriptor(&i2c_dma_resource_tx, &i2c_descriptor);
    
    dma_start_transfer_job(&i2c_dma_resource_tx);
    i2c_master_dma_set_transfer(device, address, length, I2C_TRANSFER_READ);
    
    return STATUS_OK;
}

/**
 * 
 */
enum status_code i2cdev_write_byte(struct i2c_master_module *device, 
        uint8_t address, uint8_t reg, uint8_t data)
{
    return i2cdev_write(device, address, reg, 1, &data);
}

/**
 * 
 */
enum status_code i2cdev_write_bit(struct i2c_master_module *device, 
        uint8_t address, uint8_t reg, uint8_t bit_num, uint8_t data)
{
    uint8_t byte;
    
    i2cdev_read_byte(device, address, reg, &byte);
    byte = (data != 0) ? (byte | (1 << bit_num)) : (byte & ~(1 << bit_num));
    
    return i2cdev_write_byte(device, address, reg, byte);
}

/**
 * 
 */
enum status_code i2cdev_write_bits(struct i2c_master_module *device, 
        uint8_t address, uint8_t reg, uint8_t bit_start, uint8_t length, 
        uint8_t data)
{
    uint8_t byte;
    enum status_code status;
    
    if ((status = i2cdev_read_byte(device, address, reg, &byte)) == STATUS_OK) {
        uint8_t mask = ((1 << length) - 1) << (bit_start - length + 1);
        data <<= (bit_start - length + 1);
        data &= mask;
        byte &= ~(mask);
        byte |= data;
        status = i2cdev_write_byte(device, address, reg, byte);
    }
    
    return status;
}

/**
 * 
 */
enum status_code i2cdev_write(struct i2c_master_module *device, 
        uint8_t address, uint8_t reg, uint16_t length, uint8_t *data)
{
    uint8_t *packet_data = (uint8_t *)malloc(length + 1);
    *packet_data = reg;
    memcpy((void *)(packet_data + 1), (const void *)data, (size_t)length++);
        
    //struct i2c_master_packet packet = {
        //.address         = address,     // device address
        //.data_length     = ++length,    // increment length to include register
        //.data            = packet_data, // pointer to data
        //.ten_bit_address = false,
        //.high_speed      = false,
        //.hs_master_code  = 0x0
    //};
    
    setup_dma_descriptor(&i2c_descriptor, device, length, packet_data);
    dma_add_descriptor(&i2c_dma_resource_tx, &i2c_descriptor);
    
    dma_start_transfer_job(&i2c_dma_resource_tx);
    i2c_master_dma_set_transfer(device, address, length, I2C_TRANSFER_WRITE);
    
    xSemaphoreTake(i2cdev_dma_event, portMAX_DELAY);
    
    return STATUS_OK;
    //return i2c_master_write_packet_wait(device, &packet);
}

static void transfer_done(const struct dma_resource *const resource) {
    portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
    
    UNUSED(resource);
    transfer_is_done = true;
    
    //statind_set_all();
        
    xSemaphoreGiveFromISR(i2cdev_dma_event, &xHigherPriorityTaskWoken);
    
    portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
}

static void configure_dma_resource(struct dma_resource *resource, uint8_t dma_trigger) {
    struct dma_resource_config config;
    dma_get_config_defaults(&config);
    
    config.peripheral_trigger = dma_trigger;
    config.trigger_action = DMA_TRIGGER_ACTON_BEAT;
    
    dma_allocate(resource, &config);
}

static void setup_dma_descriptor(DmacDescriptor *descriptor, 
        struct i2c_master_module *i2c_device, uint16_t length, uint8_t *data) 
{
    struct dma_descriptor_config config;
    
    dma_descriptor_get_config_defaults(&config);
    
    config.beat_size = DMA_BEAT_SIZE_BYTE;
    config.dst_increment_enable = false;
    config.block_transfer_count = length;
    config.source_address = (uint32_t)data + length;
    config.destination_address = (uint32_t)(&i2c_device->hw->I2CM.DATA.reg);
    
    dma_descriptor_create(descriptor, &config);
}