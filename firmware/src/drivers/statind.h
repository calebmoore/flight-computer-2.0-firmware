/*
 * led.h
 *
 * Created: 2/1/2015 2:11:40 PM
 *  Author: Caleb
 */ 


#ifndef STATIND_H_
#define STATIND_H_

#include <asf.h>

typedef enum { LED_STATUS_1, LED_STATUS_2, BUZZER } StatusIndicator;

#define LED_STATUS_1_PIN PIN_PB22
#define LED_STATUS_2_PIN PIN_PB23
#define BUZZER_PIN       PIN_PA11

#define STATIND_LED_ERROR        LED_STATUS_2
#define STATIND_LED_DOWNLINK     LED_STATUS_1

#define N_INDICATORS     ((int)3)

void statind_init(void);
bool statind_test(void);

void statind_clear_all(void);
void statind_set_all(void);

void statind_set(StatusIndicator indicator, bool value);

void statind_task(void *param);

#endif /* STATIND_H_ */
