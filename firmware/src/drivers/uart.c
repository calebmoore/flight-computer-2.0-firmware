/*
 * uart.c
 *
 * Created: 2/4/2015 5:44:17 PM
 *  Author: Winglee Plasma Lab
 */ 

#include <asf.h>

#include "uart.h"

static bool _is_init;

static struct usart_module _uart;


/**
 *  
 */
void uart_init() {
	struct usart_config config;
    int baudrate = UART_BAUDRATE;
    // TODO: change baudrate on the fly

    if (_is_init) {
        return;
    }

    usart_get_config_defaults(&config);
    config.baudrate = baudrate;
    config.mux_setting = UART_SERCOM_MODE;
    config.pinmux_pad0 = UART_SERCOM_PAD0_PINMUX;
    config.pinmux_pad1 = UART_SERCOM_PAD1_PINMUX;
    config.pinmux_pad2 = UART_SERCOM_PAD2_PINMUX;
    config.pinmux_pad3 = UART_SERCOM_PAD3_PINMUX;

    while(usart_init(&_uart, UART_SERCOM, &config) != STATUS_OK);

    usart_enable(&_uart);
}


/**
 *  
 */
bool uart_test() {
    return _is_init;
}


/**
 *  
 */
int uart_putchar(int ch) {
    usart_write_wait(&_uart, (const uint16_t)ch);

    return (unsigned char)ch;
}