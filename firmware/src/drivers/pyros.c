/*
 * pyros.c
 *
 * Created: 2/4/2015 8:00:15 PM
 *  Author: Winglee Plasma Lab
 */ 

#include "pyros.h"

#include "FreeRTOS.h"
#include "timers.h"
#include "semphr.h"

#include "config.h"
#include "worker.h"
#include "statseq.h"

#define N_PYROS 4

static const Pyro _pyros[] = {
    PYRO_1,
    PYRO_2,
    PYRO_3,
    PYRO_4,
    PYRO_NONE
};

static const uint8_t _pyro_fire_pins[] = {
    PYRO_1_FIRE_PIN,
    PYRO_2_FIRE_PIN,
    PYRO_3_FIRE_PIN,
    PYRO_4_FIRE_PIN
};

#ifdef ALLOW_EXTERNAL_PYRO_FIRING

static const uint8_t _pyro_external_fire_pins[] = {
    PYRO_1_EXT_FIRE_EIC_PIN,
    PYRO_2_EXT_FIRE_EIC_PIN,
    PYRO_3_EXT_FIRE_EIC_PIN,
    PYRO_4_EXT_FIRE_EIC_PIN
};

static const uint8_t _pyro_external_fire_eic_pins[] = {
    PYRO_1_EXT_FIRE_EIC_PIN,
    PYRO_2_EXT_FIRE_EIC_PIN,
    PYRO_3_EXT_FIRE_EIC_PIN,
    PYRO_4_EXT_FIRE_EIC_PIN
};

static const uint8_t _pyro_external_fire_eic_muxes[] = {
    PYRO_1_EXT_FIRE_EIC_MUX,
    PYRO_2_EXT_FIRE_EIC_MUX,
    PYRO_3_EXT_FIRE_EIC_MUX,
    PYRO_4_EXT_FIRE_EIC_MUX
};

static const uint8_t _pyro_external_fire_eic_lines[] = {
    PYRO_1_EXT_FIRE_EIC_LINE,
    PYRO_2_EXT_FIRE_EIC_LINE,
    PYRO_3_EXT_FIRE_EIC_LINE,
    PYRO_4_EXT_FIRE_EIC_LINE
};

#endif

static bool _is_init  = false;
static bool _is_armed = false;

// each pyro can only fire once, to prevent damage to hardware
static bool _fired_pyros[N_PYROS]; 

xSemaphoreHandle _firing_pyro_semphr;
xTimerHandle     _delay_timer;
xTimerHandle     _duration_timer;

static Pyro _active_pyro;

static void fire_active_pyro(TimerHandle_t xTimer);
static void disable_active_pyro(TimerHandle_t xTimer);

#ifdef ALLOW_EXTERNAL_PYRO_FIRING
static void external_fire_pyro(void *arg);
static void external_fire_callback(void);
#endif

static void set_pyro_value(Pyro pyro, bool value);


/**
 * Configures pyros and external fire pins and initializes control timers and 
 * external interrupts.
 */
void pyros_init() {
    unsigned int i = 0;
    
    if (_is_init) {
        return;
    }
    
    _active_pyro = PYRO_NONE;
    
    // configure firing pins
    struct port_config fire_config;
    port_get_config_defaults(&fire_config);
    fire_config.direction = PORT_PIN_DIR_OUTPUT;
    fire_config.input_pull = PORT_PIN_PULL_DOWN;

#ifdef ALLOW_EXTERNAL_PYRO_FIRING
    // configure external fire pins
    struct extint_chan_conf external_fire_config;
    extint_chan_get_config_defaults(&external_fire_config);
    
    // EXTI trigger on rising edge, pull downs enabled
    external_fire_config.detection_criteria = EXTINT_DETECT_HIGH;
    external_fire_config.filter_input_signal = true;
    external_fire_config.gpio_pin_pull = EXTINT_PULL_DOWN;
#endif

    // initialize each pin
    for (; i < N_PYROS; i++) {
        _fired_pyros[i] = false;
        
        // fire pin config
        port_pin_set_config(_pyro_fire_pins[i], &fire_config);
        port_pin_set_output_level(_pyro_fire_pins[i], false);

#ifdef ALLOW_EXTERNAL_PYRO_FIRING      
        // ext fire pin config
        // TODO: safety feature, alert if ext fire pin is high at startup
        external_fire_config.gpio_pin = _pyro_external_fire_eic_pins[i];
        external_fire_config.gpio_pin_mux = _pyro_external_fire_eic_muxes[i];
        extint_chan_set_config(_pyro_external_fire_eic_lines[i], &external_fire_config);
        extint_register_callback(external_fire_callback, 
                _pyro_external_fire_eic_lines[i], EXTINT_CALLBACK_TYPE_DETECT);
        extint_chan_enable_callback(_pyro_external_fire_eic_lines[i], EXTINT_CALLBACK_TYPE_DETECT);                
#endif        
    }
    
    // only fire one pyro at a time
    vSemaphoreCreateBinary(_firing_pyro_semphr);
    
    _delay_timer = xTimerCreate((const char *)"py delay", 1, pdFALSE, NULL, fire_active_pyro);
    _duration_timer = xTimerCreate((const char *)"py dur", M2T(PYRO_DURATION), pdFALSE, NULL, disable_active_pyro);
    
    _is_init = true;
}


/**
 * Whether pyros have been initialized.
 */
bool pyros_test() {
    return _is_init;
}


/**
 * Arm pyros for firing. Calls to pyros_fire_pyro() or similar will do nothing
 * without this being called first.
 */
void pyros_arm() {
    _is_armed = true;
}


/**
 * Disarm pyros.
 */
void pyros_disarm() {
    _is_armed = false;
}


/**
 * Whether the pyros have been armed and readied for firing.
 */
bool pyros_is_armed() {
    return _is_armed;
}


/**
 * Fire the passed pyro.
 *
 * @param pyro pyro to fire
 */
void pyros_fire_pyro(Pyro pyro) {
    pyros_fire_pyro_wait(pyro, 0);
}


/**
 * Fire the passed pyro after a delay.
 *
 * @param pyro  pyro to fire
 * @param delay ignition delay in ms
 */
void pyros_fire_pyro_wait(Pyro pyro, uint16_t delay) {
    if (!_is_armed) {
        return;
    }
    
    xSemaphoreTake(_firing_pyro_semphr, portMAX_DELAY);
    
    statseq_run(LED_STATUS_2, seq_firing);
    statseq_run(BUZZER, seq_firing);
    
    _fired_pyros[pyro] = true;
    _active_pyro = pyro;
    
    if (delay) {
        xTimerChangePeriod(_delay_timer, M2T(delay), 0);
        xTimerReset(_delay_timer, 0);
    } else {
        set_pyro_value(_active_pyro, true);
        xTimerReset(_duration_timer, 0);       
    }
}


/**
 * Powers the active pyro and starts the timer that will unpower it.
 *
 * @param xTimer handle to FreeRTOS timer calling this callback
 */
static void fire_active_pyro(TimerHandle_t xTimer) {
    set_pyro_value(_active_pyro, true);
    xTimerReset(_duration_timer, 0);
}


/**
 * Disables the active pyro.
 *
 * @param xTimer handle to FreeRTOS timer calling this callback
 */
static void disable_active_pyro(TimerHandle_t xTimer) {
    set_pyro_value(_active_pyro, false);
    _active_pyro = PYRO_NONE;
    
    statseq_stop(LED_STATUS_2, seq_firing);
    statseq_stop(BUZZER, seq_firing);
    
    xSemaphoreGive(_firing_pyro_semphr);
}


/**
 * Sets or clears discrete output connected to pyro.
 *
 * @param pyro pyro to set/clear
 * @param value true for set, false for clear
 */
static void set_pyro_value(Pyro pyro, bool value) {
    port_pin_set_output_level(_pyro_fire_pins[pyro], value);
}


#ifdef ALLOW_EXTERNAL_PYRO_FIRING

/**
 * Worker loop callback that will fire the requested pyro.
 *
 * @param arg pyro to fire
 */
static void external_fire_pyro(void *arg) {
    Pyro *p = (Pyro *)arg;
    pyros_fire_pyro(*p);
}


/**
 * ISR for external fire interrupts.
 */
static void external_fire_callback() {
    unsigned int i = 0;
    
    // find triggered pyro, ignore already triggered pyros
    for (; i < N_PYROS; i++) {
        if (_fired_pyros[i]) {
            continue;
        }
        
        if (port_pin_get_input_level(_pyro_external_fire_pins[i])) {
            // fire the pyro
            worker_schedule_job_from_isr(external_fire_pyro, (void *)&_pyros[i]);
            
            // prevent from firing again
            extint_chan_disable_callback(_pyro_external_fire_eic_lines[i], EXTINT_CALLBACK_TYPE_DETECT);
            
            break;
        }
    }
}

#endif