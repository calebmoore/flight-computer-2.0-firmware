/*
* radiouart.c
*
* Created: 2/20/2015 5:48:38 PM
*  Author: Caleb
*/ 

#include <asf.h>

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

#include "radiouart.h"

#define RADIO_UART_DATA_TIMEOUT_MS    1000
#define RADIO_UART_DATA_TIMEOUT_TICKS M2T(RADIO_UART_DATA_TIMEOUT_MS)
#define RADIO_UART_QUEUE_SIZE         40

static bool _is_init;

static struct usart_module _radio;

xSemaphoreHandle _tx_complete_semphr = NULL;
static xQueueHandle _radio_queue;

static void configure_uart(void);
static void uart_isr(uint8_t instance);

// TODO: DMA transfer

/**
 * Opens the radio usart module, creates reception queue, and configures the
 * ISR.
 */
void radio_uart_init() {  
    if(_is_init) {
        return;
    }

    configure_uart();

    vSemaphoreCreateBinary(_tx_complete_semphr);

    _radio_queue = xQueueCreate(RADIO_UART_QUEUE_SIZE, sizeof(uint8_t));

    _is_init = true;
}
 

/**
 * Tests that the radio uart module was initiated. 
 */
bool radio_uart_test() {
    return _is_init;
}

/**
 * Attempt to receive a character from the radio. Blocks for predetermined
 * amount of time (defined as RADIO_UART_DATA_TIMEOUT_TICKS).
 *
 * @param:out byte to load with received data
 * @return true if data received, false if timed out
 */
bool radio_uart_get_data_with_timeout(uint8_t *c) {
    return xQueueReceive(_radio_queue, c, RADIO_UART_DATA_TIMEOUT_TICKS) == pdTRUE;
} 

/**
 * Synchronously send data over the radio. Blocks until complete.
 *
 * @param:in size number of bytes to send
 * @param:in location of data to send 
 */
void radio_uart_send_data_wait(uint16_t size, uint8_t *data) {
    if (!_is_init) {
        return;
    }

    usart_write_buffer_wait(&_radio, (const uint8_t *)data, size);
}

/**
 * Asynchronously send data over the radio.
 *
 * @param:in size number of bytes to send
 * @param:in location of data to send 
 */
void radio_uart_send_data_isr_blocking(uint16_t size, uint8_t *data) {
    if (!_is_init) {
        return;
    }

    xSemaphoreTake(_tx_complete_semphr, portMAX_DELAY);
    usart_write_buffer_job(&_radio, data, size);
}

/**
 * Configures the SERCOM module as a USART module. 
 */
static void configure_uart() {
    struct usart_config config;
    uint8_t instance_index;

    // configure SERCOM
    usart_get_config_defaults(&config);

    config.baudrate = RADIO_UART_BAUDRATE;
    config.mux_setting = RADIO_UART_SERCOM_MODE;
    config.pinmux_pad0 = RADIO_UART_PINMUX_PAD0;
    config.pinmux_pad1 = RADIO_UART_PINMUX_PAD1;
    config.pinmux_pad2 = RADIO_UART_PINMUX_PAD2;
    config.pinmux_pad3 = RADIO_UART_PINMUX_PAD3;
     
    while(usart_init(&_radio, RADIO_UART_SERCOM, &config) != STATUS_OK);

    // inject custom interrupt handler
    instance_index = _sercom_get_sercom_inst_index(RADIO_UART_SERCOM);
    _sercom_set_handler(instance_index, uart_isr);

    // enable radio and tranceivers
    usart_enable(&_radio);
    usart_enable_transceiver(&_radio, USART_TRANSCEIVER_TX);
    usart_enable_transceiver(&_radio, USART_TRANSCEIVER_RX);
    
    // enable ISR
    ((SercomUsart *)RADIO_UART_SERCOM)->INTENSET.reg = 
            SERCOM_USART_INTFLAG_RXC | 
            SERCOM_USART_INTFLAG_TXC | 
            SERCOM_USART_INTFLAG_DRE;
}

/**
 * ISR for the radio usart module.
 *
 * ASF callbacks are not used since passive reception of incoming data is
 * required to assemble the packets. The ISR is based on the one used to drive
 * ASF's callback functions (_usart_interrupt_handler() in 
 * /ASF/sam0/drivers/sercom/usart/usart_interrupt.c) augmented with needed
 * RTOS features.
 *
 * Incoming bytes are passed to a FreeRTOS queue where they are then pieced 
 * together to form a packet in the radiolink task. 
 *
 * Outgoing packets are sent asynchronously one byte at a time.
 *
 * @param instance index of SERCOM module 
 */
static void uart_isr(uint8_t instance) {
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    struct usart_module *module = (struct usart_module *)_sercom_instances[instance];
    SercomUsart *const usart_hw = &(module->hw->USART);
    uint16_t interrupt_status;
    uint16_t data;
    uint8_t error_code;

    // wait until USART sync
    _usart_wait_for_sync(module);
    
    // read and mask interrupt flag register
    interrupt_status = usart_hw->INTFLAG.reg;
    interrupt_status &= usart_hw->INTENSET.reg;

    // check for USART reception complete
    if (interrupt_status & SERCOM_USART_INTFLAG_RXC) {
        error_code = (uint8_t)(usart_hw->STATUS.reg & SERCOM_USART_STATUS_MASK);

        // check for errors
        if (error_code) {
            // only frame errors or buffer overloads should be possible
            if (error_code & 
                    (SERCOM_USART_STATUS_FERR | SERCOM_USART_STATUS_BUFOVF)) 
            {
                usart_hw->STATUS.reg = SERCOM_USART_STATUS_FERR | SERCOM_USART_STATUS_BUFOVF;    
            }
        } else {
            // everything is fine, push received char into queue
            data = (usart_hw->DATA.reg & SERCOM_USART_DATA_MASK);

            if (!xQueueSendFromISR(_radio_queue, (uint8_t *)&data,
                    &xHigherPriorityTaskWoken))
            {
                // TODO: handle enqueueing error
                // error enqueueing data
            }
        }
    } 
    
    // check if data transmission is complete
    if (interrupt_status & SERCOM_USART_INTFLAG_TXC) {
        // clear interrupt flag, set status to ok
        usart_hw->INTENCLR.reg = SERCOM_USART_INTFLAG_TXC;
        module->tx_status = STATUS_OK;

        // give up tx semaphore
        xSemaphoreGiveFromISR(_tx_complete_semphr, &xHigherPriorityTaskWoken);
    } 
    
    // check if DRE interrupt is set, send remaining data if it is
    if (interrupt_status & SERCOM_USART_INTFLAG_DRE) {
        // check for remaining data
        if (module->remaining_tx_buffer_length) {
            // if there is still data to send
            uint8_t data_to_send = *(module->tx_buffer_ptr);
            (module->tx_buffer_ptr)++;

            // send data
            usart_hw->DATA.reg = (data_to_send & SERCOM_USART_DATA_MASK);

            if (--(module->remaining_tx_buffer_length) == 0) {
                // disable Data Register Empty interrupt
                usart_hw->INTENCLR.reg = SERCOM_USART_INTFLAG_DRE;

                // enable Transmission Complete interrupt
                usart_hw->INTENSET.reg = SERCOM_USART_INTFLAG_TXC;
            }
        } else {
            // no more data, clear DRE flag
            usart_hw->INTENCLR.reg = SERCOM_USART_INTFLAG_DRE;
        }
    }

    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}