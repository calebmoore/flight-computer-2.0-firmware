/*
 * led.c
 *
 * Created: 2/1/2015 2:11:51 PM
 *  Author: Caleb
 */ 

#include <asf.h>

#include "FreeRTOS.h"
#include "task.h"

#include "statind.h"

static const StatusIndicator _indicators[] = {
    LED_STATUS_1,
    LED_STATUS_2,
    BUZZER
};

static const uint8_t _indicator_pins[] = { 
	LED_STATUS_1_PIN, 
	LED_STATUS_2_PIN,
    BUZZER_PIN 
};

#define ACTIVE_HIGH true
#define ACTIVE_LOW  false
static const bool _indicator_polarities[] = {
    ACTIVE_LOW,
    ACTIVE_LOW,
    ACTIVE_HIGH
};

static bool _is_init = false;


/**
 *  
 */
void statind_init() {	
    unsigned int i = 0;
    
	if (_is_init) {
		return;
	}
	
	// set indicator pins as outputs	
	struct port_config config;
	port_get_config_defaults(&config);
	
	config.direction = PORT_PIN_DIR_OUTPUT;
	
    // initialize each status indicator
    for (; i < N_INDICATORS; i++) {
	    port_pin_set_config(_indicator_pins[i], &config);
	    statind_set(_indicators[i], false);
    }        
	
	_is_init = true;
}


/**
 *  
 */
bool statind_test() {
	statind_set(LED_STATUS_1, true);
	statind_set(LED_STATUS_2, false);
    //statind_set(BUZZER, false);
	
	vTaskDelay(M2T(250));
	
	statind_set(LED_STATUS_1, false);
	statind_set(LED_STATUS_2, true);
    //statind_set(BUZZER, true);
	
	vTaskDelay(M2T(250));
	
	statind_clear_all();
	
	return _is_init;
}


/**
 *  
 */
void statind_set_all() {
	unsigned int i = 0;
    for (; i < N_INDICATORS; i++) {
        statind_set(_indicators[i], true);
    }
}


/**
 *  
 */
void statind_clear_all() {
	int i = 0;
	for (; i < N_INDICATORS; i++) {
    	statind_set(_indicators[i], false);
	}
}


/**
 *  
 */
void statind_set(StatusIndicator indicator, bool value) {
	if (indicator >= N_INDICATORS) {
		return;
	}
	
    if (_indicator_polarities[indicator] == ACTIVE_LOW) {
        value = !value;
    }
    
	port_pin_set_output_level(_indicator_pins[indicator], (const bool)value);
}