/*
 * ledseq.h
 *
 * Created: 2/2/2015 7:28:00 PM
 *  Author: Winglee Plasma Lab
 */ 


#ifndef LEDSEQ_H_
#define LEDSEQ_H_

#include <asf.h>

#include <statind.h>

#define STATSEQ_WAIT_MS(x) (x)
#define STATSEQ_STOP       (-1)
#define STATSEQ_LOOP       (-2)

typedef struct StatusSequenceStep {
    bool value;
    int action;
} StatusSequenceStep;    

void statseq_init(void);
bool statseq_test(void);

void statseq_enable(bool enable);
void statseq_run(StatusIndicator indicator, StatusSequenceStep *sequence);
void statseq_stop(StatusIndicator indicator, StatusSequenceStep *sequence);
void statseq_set_times(StatusSequenceStep *sequence, 
        int32_t on_duration, int32_t off_duration);

// premade sequences
extern StatusSequenceStep seq_armed[];
extern StatusSequenceStep seq_calibrated[];
extern StatusSequenceStep seq_test_passed[];
extern StatusSequenceStep seq_firing[];
extern StatusSequenceStep seq_error[];
extern StatusSequenceStep seq_blip[];

#endif /* LEDSEQ_H_ */