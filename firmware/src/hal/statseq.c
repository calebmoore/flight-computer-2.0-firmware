/*
 * ledseq.c
 *
 * Created: 2/2/2015 7:27:49 PM
 *  Author: Winglee Plasma Lab
 */ 

#include <asf.h>

#include "FreeRTOS.h"
#include "timers.h"
#include "semphr.h"

#include "statind.h"
#include "statseq.h"

// sequence priority
static StatusSequenceStep *sequences[] = {
    seq_test_passed,
    seq_blip,
    seq_firing,
    seq_error,
    seq_armed,
    seq_calibrated
};

StatusSequenceStep seq_armed[] = {
    {  true, STATSEQ_WAIT_MS(100) },
    { false, STATSEQ_WAIT_MS(900)},
    {     0, STATSEQ_LOOP }
};

StatusSequenceStep seq_calibrated[] = {
    {  true, STATSEQ_WAIT_MS(50) },
    { false, STATSEQ_WAIT_MS(50) },
    {  true, STATSEQ_WAIT_MS(50) },
    { false, STATSEQ_WAIT_MS(450) },
    {     0, STATSEQ_LOOP }
};

StatusSequenceStep seq_test_passed[] = {
    {  true, STATSEQ_WAIT_MS(100) },
    { false, STATSEQ_WAIT_MS(100) },
    {  true, STATSEQ_WAIT_MS(100) },
    { false, STATSEQ_WAIT_MS(100) },
    {  true, STATSEQ_WAIT_MS(100) },
    { false, STATSEQ_WAIT_MS(100) },
    {     0, STATSEQ_STOP }
};

StatusSequenceStep seq_firing[] = {
    {  true, STATSEQ_WAIT_MS(1000)},
    { false, STATSEQ_WAIT_MS(250) },
    {     0, STATSEQ_LOOP }
};

StatusSequenceStep seq_error[] = {
    {  true, STATSEQ_WAIT_MS(50) },
    { false, STATSEQ_WAIT_MS(50) },
    {     0, STATSEQ_LOOP }
};

StatusSequenceStep seq_blip[] = {
    {  true, STATSEQ_WAIT_MS(1) },
    { false, STATSEQ_WAIT_MS(0) },
    {     0, STATSEQ_STOP       }
};

#define N_SEQUENCES (int)(sizeof(sequences)/sizeof(sequences[0]))

#define SEQUENCE_INVALID (-1)

static void run_sequence(xTimerHandle xTimer);
static int get_priority(StatusSequenceStep *sequence);
static void update_active(StatusIndicator indicator);

static int state[N_INDICATORS][N_SEQUENCES];
static int active_sequence[N_INDICATORS];

static xTimerHandle timer[N_INDICATORS];

static xSemaphoreHandle statseq_semphr;

static bool _is_init;
static bool statseq_enabled;

void statseq_init() {
    unsigned int i, j;
    
    if (_is_init) {
        return;
    }
    
    statind_init();
    
    // initialize sequence state
    for (i = 0; i < N_INDICATORS; i++) {
        active_sequence[i] = STATSEQ_STOP;
        
        for (j = 0; j < N_SEQUENCES; j++) {
            state[i][j] = STATSEQ_STOP;
        }
    }
    
    // initiate software timers
    for (i = 0; i < N_INDICATORS; i++) {
        timer[i] = xTimerCreate((const char *)"statseq_timer",
                M2T(1000), pdFALSE, (void *)i, run_sequence);
    }
    
    vSemaphoreCreateBinary(statseq_semphr);
    
    _is_init = true;
}

bool statseq_test() {
    bool status;
    
    status = _is_init & statind_test();
    statseq_enable(true);
    
    return status;
}

void statseq_enable(bool enable) {
    statseq_enabled = enable;
}

void statseq_run(StatusIndicator indicator, StatusSequenceStep *sequence) {
    int pri = get_priority(sequence);
    
    if (pri == SEQUENCE_INVALID) {
        return;
    }
    
    xSemaphoreTake(statseq_semphr, portMAX_DELAY);
    
    // reset sequence
    state[indicator][pri] = 0;
    update_active(indicator);
    
    xSemaphoreGive(statseq_semphr);
    
    // run first step if new sequence is the active sequence
    if (active_sequence[indicator] == pri) {
        run_sequence(timer[indicator]);
    }
}

void statseq_set_times(StatusSequenceStep *sequence, int32_t on_duration, int32_t off_duration) {
    sequence[0].action = on_duration;
    sequence[1].action = off_duration;
}

void statseq_stop(StatusIndicator indicator, StatusSequenceStep *sequence) {
    int pri = get_priority(sequence);
    
    if (pri == SEQUENCE_INVALID) {
        return;
    }
    
    xSemaphoreTake(statseq_semphr, portMAX_DELAY);
    state[indicator][pri] = STATSEQ_STOP;
    update_active(indicator);
    xSemaphoreGive(statseq_semphr);
    
    run_sequence(timer[indicator]);
}

// TODO: fix -W-bad-function-cast
static void run_sequence(xTimerHandle xTimer) {
    StatusIndicator indicator = (StatusIndicator)pvTimerGetTimerID(xTimer);
    StatusSequenceStep *step;
    bool leave = false;
    
    if (!statseq_enabled) {
        return;
    }
    
    while (!leave) {
        int pri = active_sequence[indicator];
        
        if (pri == STATSEQ_STOP) {
            return;
        }
        
        step = &sequences[pri][state[indicator][pri]];
        state[indicator][pri]++;
        
        xSemaphoreTake(statseq_semphr, portMAX_DELAY);
        
        switch(step->action) {
            case STATSEQ_LOOP:
                state[indicator][pri] = 0;
                break;
            case STATSEQ_STOP:
                state[indicator][pri] = STATSEQ_STOP;
                update_active(indicator);
                break;
            default:
                statind_set(indicator, step->value);
                if (step->action == 0)
                    break;
                xTimerChangePeriod(xTimer, M2T(step->action), 0);
                xTimerStart(xTimer, 0);
                leave = true;
                break;
        }
        
        xSemaphoreGive(statseq_semphr);
    }
}

static int get_priority(StatusSequenceStep *sequence) {
    int pri;
    
    // find priority of sequence
    for (pri = 0; pri < N_SEQUENCES; pri++) {
        if (sequences[pri] == sequence) {
            return pri;
        }
    }
    
    return SEQUENCE_INVALID; // sequence invalid
}

static void update_active(StatusIndicator indicator) {
    int pri;
    
    active_sequence[indicator] = STATSEQ_STOP;
    statind_set(indicator, false);
    
    for (pri = 0; pri < N_SEQUENCES; pri++) {
        if (state[indicator][pri] != STATSEQ_STOP) {
            active_sequence[indicator] = pri;
            break;
        }
    }
}