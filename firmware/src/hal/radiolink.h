/*
 * radiolink.h
 *
 * Created: 2/6/2015 6:27:31 PM
 *  Author: Winglee Plasma Lab
 */ 


#ifndef RADIOLINK_H_
#define RADIOLINK_H_

#include "telemetry.h"

#define RADIO_LINK_MTU           32   // maximum transmissible unit
#define RADIO_LINK_START_BYTE_1  0x43 // C
#define RADIO_LINK_START_BYTE_2  0x4D // M
#define RADIO_LINK_HEADER(ch, t) (((t & 0x0F) << 4) | (ch & 0x0F))

typedef enum {
    RADIO_LINK_STATUS_WAIT_FOR_START_1,
    RADIO_LINK_STATUS_WAIT_FOR_START_2,
    RADIO_LINK_STATUS_WAIT_FOR_HEADER,
    RADIO_LINK_STATUS_WAIT_FOR_LENGTH,
    RADIO_LINK_STATUS_WAIT_FOR_DATA,
    RADIO_LINK_STATUS_WAIT_FOR_CHK_1,
    RADIO_LINK_STATUS_WAIT_FOR_CHK_2
} RadioLinkRxStatus;

typedef struct RadioLinkPacket {
    union {
        uint8_t header;
        struct {
            uint8_t channel :4;
            uint8_t type    :4;   
        };
    };
    uint8_t length;
    uint8_t data[RADIO_LINK_MTU];
} __attribute__((packed)) RadioLinkPacket;

void radiolink_init(void);
bool radiolink_test(void);

void radiolink_set_channel(uint8_t channel);

void radiolink_dispatch(RadioLinkPacket *p);

struct telemetry_link_operations *radiolink_get_link(void);

#endif /* RADIOLINK_H_ */