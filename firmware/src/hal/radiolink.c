/*
 * radiolink.c
 *
 * Created: 2/6/2015 6:27:21 PM
 *  Author: Winglee Plasma Lab
 */ 

#include <asf.h>

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"

#include "debug.h"
#include "uart.h"

#include "config.h"
#include "radiouart.h"

#include "telemetry.h"
#include "statind.h"
#include "statseq.h"
#include "fwassert.h"

#include "radiolink.h"

#define RADIO_LINK_TX_QUEUE_SIZE  1
#define RADIO_LINK_TLM_QUEUE_SIZE 5

#define RADIO_LINK_DEFAULT_CHANNEL 0x00
#define RADIO_LINK_RADIO_RAW       0x00

static xQueueHandle _tlm_packet_delivery;
static xQueueHandle _tx_queue;
static xSemaphoreHandle _link_access_semphr;
static RadioLinkPacket _tx_packet;

static uint8_t send_buffer[64];

static bool _is_init;
static uint8_t _rssi;         // RSSI from radio
static uint8_t _link_channel; // radio link channel (0 - 15)

static int radiolink_send_telemetry_packet(TelemetryPacket *p);
static int radiolink_set_enable(bool enable);
static int radiolink_receive_telemetry_packet(TelemetryPacket *p);

static void radiolink_task(void *param);

static struct telemetry_link_operations radiolink_operations = {
    .set_enable     = radiolink_set_enable,
    .send_packet    = radiolink_send_telemetry_packet,
    .receive_packet = radiolink_receive_telemetry_packet
};

static int send_radio_link_packet(RadioLinkPacket *p);
static void build_packet(uint8_t c, RadioLinkRxStatus *rx_status, 
        uint8_t *data_index, uint8_t *chksum, RadioLinkPacket *p);
static void route_incoming_packet(RadioLinkPacket *p);

/**
 *  
 */
void radiolink_init() {
    if (_is_init) {
        return;
    }
    
    _link_channel = RADIO_LINK_DEFAULT_CHANNEL;
    radio_uart_init();

    _tx_queue = xQueueCreate(RADIO_LINK_TX_QUEUE_SIZE, 
            sizeof(RadioLinkPacket));

    _tlm_packet_delivery = xQueueCreate(RADIO_LINK_TLM_QUEUE_SIZE, 
            sizeof(TelemetryPacket));

    vSemaphoreCreateBinary(_link_access_semphr);

    if (xTaskCreate(radiolink_task, (const char* const)TASK_NAME_RADIO_LINK,
            TASK_STACK_SIZE_RADIO_LINK, NULL, TASK_PRIORITY_RADIO_LINK, NULL) == pdPASS)
    {
	    _is_init = true;  
    }
}


/**
 *  
 */
bool radiolink_test() {
	return _is_init && radio_uart_test();
}


/**
 *  
 */
struct telemetry_link_operations *radiolink_get_link() {
	return &radiolink_operations;
}


/**
 *  
 */
static int radiolink_send_telemetry_packet(TelemetryPacket *p) {
    RadioLinkPacket rlp;

    ASSERT(p->size <= TLM_MAX_DATA_SIZE);

    rlp.channel = _link_channel;
    rlp.type = RADIO_LINK_RADIO_RAW;
    rlp.length = p->size + 1;
    memcpy(rlp.data, &p->header, p->size + 1);

	return send_radio_link_packet(&rlp);
}


void radiolink_set_channel(uint8_t channel) {
    if (channel <= 0x0F) {
        _link_channel = channel & 0x0F;
    }
}


/**
 *  
 */
static int radiolink_set_enable(bool enable) {
	return 0;
}


/**
 *  
 */
static int radiolink_receive_telemetry_packet(TelemetryPacket *p) {
	if (xQueueReceive(_tlm_packet_delivery, p, M2T(100)) == pdTRUE) {
        return 0;
    }

    return -1;
}


static void radiolink_task(void *param) {
    RadioLinkRxStatus rx_status = RADIO_LINK_STATUS_WAIT_FOR_START_1;
    RadioLinkPacket p;
    uint8_t c;
    uint8_t data_index = 0;
    uint8_t chksum[2] = {0};
    uint8_t counter = 0;

    while (1) {
        // wait for received data
        if (radio_uart_get_data_with_timeout(&c)) {
            counter++;
            build_packet(c, &rx_status, &data_index, chksum, &p);
        } else {
            // queue timeout
            rx_status = RADIO_LINK_STATUS_WAIT_FOR_START_1;
        }
    }
}


static int send_radio_link_packet(RadioLinkPacket *p) {
    int i = 0;
    int data_size;
    uint8_t chksum[2] = {0};

    xSemaphoreTake(_link_access_semphr, portMAX_DELAY);
    
    ASSERT(p->length <= RADIO_LINK_MTU);
    
    send_buffer[0] = RADIO_LINK_START_BYTE_1;
    send_buffer[1] = RADIO_LINK_START_BYTE_2;
    send_buffer[2] = p->header;
    send_buffer[3] = p->length;
    
    memcpy(&send_buffer[4], p->data, p->length);
    data_size = p->length + 6;
    
    // calculate checksum
    for (i = 2; i < data_size - 2; i++) {
        chksum[0] += send_buffer[i];
        chksum[1] += chksum[0];
    }
    
    send_buffer[data_size - 2] = chksum[0];
    send_buffer[data_size - 1] = chksum[1];
    
    // blinkenlights
    statseq_run(STATIND_LED_DOWNLINK, seq_blip);
    
    // send data over radio
    radio_uart_send_data_isr_blocking(data_size, send_buffer);
    xSemaphoreGive(_link_access_semphr);

    return true;
}


/**
 * Creates a RadioLinkPacket from incoming bytes.
 *
 * @param:in  incoming byte
 * @param:out current parser status
 * @param:out received packet payload index
 * @param:out checksum
 * @oaram:out packet to load
 */
static void build_packet(uint8_t c, RadioLinkRxStatus *rx_status, 
        uint8_t *data_index, uint8_t *chksum, RadioLinkPacket *p) 
{
    switch(*rx_status) {
    case RADIO_LINK_STATUS_WAIT_FOR_START_1:
        *rx_status = (c == RADIO_LINK_START_BYTE_1) ?
                RADIO_LINK_STATUS_WAIT_FOR_START_2 :
                RADIO_LINK_STATUS_WAIT_FOR_START_1;
        break;

    case RADIO_LINK_STATUS_WAIT_FOR_START_2:
        *rx_status = (c == RADIO_LINK_START_BYTE_2) ?
                RADIO_LINK_STATUS_WAIT_FOR_HEADER :
                RADIO_LINK_STATUS_WAIT_FOR_START_1;
        break;

    case RADIO_LINK_STATUS_WAIT_FOR_HEADER:
        DEBUG_PRINT("Packet received... ");
        chksum[0] = chksum[1] = c;
        p->header = c;
        *rx_status = RADIO_LINK_STATUS_WAIT_FOR_LENGTH;
        break;

    case RADIO_LINK_STATUS_WAIT_FOR_LENGTH:
        if (c <= RADIO_LINK_MTU) {
            p->length = c;
            chksum[0] += c;
            chksum[1] += chksum[0];
            *data_index = 0;
            *rx_status = (c > 0) ?
                    RADIO_LINK_STATUS_WAIT_FOR_DATA :
                    RADIO_LINK_STATUS_WAIT_FOR_CHK_1;
        } else {
            *rx_status = RADIO_LINK_STATUS_WAIT_FOR_START_1;
        }
        break;

    case RADIO_LINK_STATUS_WAIT_FOR_DATA:
        p->data[(*data_index)++] = c;
        chksum[0] += c;
        chksum[1] += chksum[0];
        if (*data_index == p->length) {
            *rx_status = RADIO_LINK_STATUS_WAIT_FOR_CHK_1;
        }
        break;

    case RADIO_LINK_STATUS_WAIT_FOR_CHK_1:
        if (chksum[0] == c) {
            *rx_status = RADIO_LINK_STATUS_WAIT_FOR_CHK_2;
        } else {
            // checksum error
            DEBUG_PRINT("Cheksum fail: %X %X\n", chksum[0], chksum[1]);
            *rx_status = RADIO_LINK_STATUS_WAIT_FOR_START_1;
        }
        break;

    case RADIO_LINK_STATUS_WAIT_FOR_CHK_2:
        if (chksum[1] == c) {
            // packet complete, route to appropriate module
            route_incoming_packet(p);
        }
        *rx_status = RADIO_LINK_STATUS_WAIT_FOR_START_1;
        break;

    default:
        ASSERT(0);
        break;
    }
}


 static void route_incoming_packet(RadioLinkPacket *p) {
    uint8_t group_type;

    DEBUG_PRINT("Success. \n");
    DEBUG_PRINT("Routing... \n");

    radiolink_dispatch(p);
    statseq_run(LED_STATUS_1, seq_blip);

    //if (xQueueReceive(_tx_queue, &_tx_packet, 0) == pdTRUE) {
        //radiolink_send
    //}
}


void radiolink_dispatch(RadioLinkPacket *p) {
    if (p->type == RADIO_LINK_RADIO_RAW) {
        DEBUG_PRINT("Packet is type RADIO_RAW\n");
        p->length--;
        xQueueSend(_tlm_packet_delivery, &p->length, 0);
    }
}