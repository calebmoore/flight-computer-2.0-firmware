/*******************************************************************************
 * Tracealyzer v2.5.1 Recorder Library
 * Percepio AB, www.percepio.com
 *
 * trcKernelPort.c
 *
 * Kernel-specific functionality for SafeRTOS, used by the recorder library.
 *
 * Terms of Use
 * This software is copyright Percepio AB. The recorder library is free for
 * use together with Percepio products. You may distribute the recorder library
 * in its original form, including modifications in trcHardwarePort.c/.h
 * given that these modification are clearly marked as your own modifications
 * and documented in the initial comment section of these source files.
 * This software is the intellectual property of Percepio AB and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by Percepio AB.
 *
 * Disclaimer
 * The trace tool and recorder library is being delivered to you AS IS and
 * Percepio AB makes no warranty as to its use or performance. Percepio AB does
 * not and cannot warrant the performance or results you may obtain by using the
 * software or documentation. Percepio AB make no warranties, express or
 * implied, as to noninfringement of third party rights, merchantability, or
 * fitness for any particular purpose. In no event will Percepio AB, its
 * technology partners, or distributors be liable to you for any consequential,
 * incidental or special damages, including any lost profits or lost savings,
 * even if a representative of Percepio AB has been advised of the possibility
 * of such damages, or for any claim by any third party. Some jurisdictions do
 * not allow the exclusion or limitation of incidental, consequential or special
 * damages, or the exclusion of implied warranties or limitations on how long an
 * implied warranty may last, so the above limitations may not apply to you.
 *
 * Copyright Percepio AB, 2013.
 * www.percepio.com
 ******************************************************************************/

#define KERNEL_SOURCE_FILE

#include "trcConfig.h"
#include "trcKernel.h"

#if (USE_TRACEALYZER_RECORDER == 1)

#include <stdint.h>
#include <stdarg.h>
#include "task.h"
#include "queue.h"

extern unsigned char uxQueueGetQueueNumber(void*);
extern unsigned char uxQueueGetQueueType(void*);

objectHandleType prvTraceGetObjectNumber(void* handle)
{
	return uxQueueGetQueueNumber(handle);
}

objectHandleType prvTraceGetTaskNumber(void* handle)
{
	return (objectHandleType)uxTaskGetTaskNumber(handle);
}

void prvTraceEnterCritical()
{
	taskENTER_CRITICAL();
}

void prvTraceExitCritical()
{
	taskEXIT_CRITICAL();
}

unsigned char prvTraceIsSchedulerActive()
{
	return xTaskIsSchedulerSuspended() == pdFALSE;
}

unsigned char prvTraceIsSchedulerSuspended()
{
	return xTaskIsSchedulerSuspended() == pdTRUE;
}

unsigned char prvTraceIsSchedulerStarted()
{
	return xTaskIsSchedulerStarted() == pdTRUE;
}

void* prvTraceGetCurrentTaskHandle()
{
	return xTaskGetCurrentTaskHandle();
}

/* Initialization of the object property table */
void vTraceInitObjectPropertyTable()
{
	RecorderDataPtr->ObjectPropertyTable.NumberOfObjectClasses = TRACE_NCLASSES;
	RecorderDataPtr->ObjectPropertyTable.NumberOfObjectsPerClass[0] = NQueue;
	RecorderDataPtr->ObjectPropertyTable.NumberOfObjectsPerClass[1] = NTask;
	RecorderDataPtr->ObjectPropertyTable.NumberOfObjectsPerClass[2] = NISR;
	RecorderDataPtr->ObjectPropertyTable.NameLengthPerClass[0] = NameLenQueue;
	RecorderDataPtr->ObjectPropertyTable.NameLengthPerClass[1] = NameLenTask;
	RecorderDataPtr->ObjectPropertyTable.NameLengthPerClass[2] = NameLenISR;
	RecorderDataPtr->ObjectPropertyTable.TotalPropertyBytesPerClass[0] = PropertyTableSizeQueue;
	RecorderDataPtr->ObjectPropertyTable.TotalPropertyBytesPerClass[1] = PropertyTableSizeTask;
	RecorderDataPtr->ObjectPropertyTable.TotalPropertyBytesPerClass[2] = PropertyTableSizeISR;
	RecorderDataPtr->ObjectPropertyTable.StartIndexOfClass[0] = StartIndexQueue;
	RecorderDataPtr->ObjectPropertyTable.StartIndexOfClass[1] = StartIndexTask;
	RecorderDataPtr->ObjectPropertyTable.StartIndexOfClass[2] = StartIndexISR;
	RecorderDataPtr->ObjectPropertyTable.ObjectPropertyTableSizeInBytes = TRACE_OBJECT_TABLE_SIZE;
}

/* Initialization of the handle mechanism, see e.g, xTraceGetObjectHandle */
void vTraceInitObjectHandleStack()
{
	objectHandleStacks.indexOfNextAvailableHandle[0] = 0;
	objectHandleStacks.indexOfNextAvailableHandle[1] = NQueue;
	objectHandleStacks.indexOfNextAvailableHandle[2] = NQueue + NTask;
	objectHandleStacks.lowestIndexOfClass[0] = 0;
	objectHandleStacks.lowestIndexOfClass[1] = NQueue;
	objectHandleStacks.lowestIndexOfClass[2] = NQueue + NTask;
    objectHandleStacks.highestIndexOfClass[0] = NQueue - 1;
	objectHandleStacks.highestIndexOfClass[1] = NQueue + NTask - 1;
	objectHandleStacks.highestIndexOfClass[2] = NQueue + NTask + NISR - 1;
}

/* Returns the "Not enough handles" error message for this object class */
const char* pszTraceGetErrorNotEnoughHandles(traceObjectClass objectclass)
{
	switch(objectclass)
	{
	case TRACE_CLASS_TASK:
		return "Not enough TASK handles - increase NTask in trcConfig.h";
	case TRACE_CLASS_ISR:
		return "Not enough ISR handles - increase NISR in trcConfig.h";
	case TRACE_CLASS_QUEUE:
		return "Not enough QUEUE handles - increase NQueue in trcConfig.h";
	default:
		return "pszTraceGetErrorNotEnoughHandles: Invalid objectclass!";
	}
}

/* Returns the exclude state of the object */
uint8_t uiTraceIsObjectExcluded(traceObjectClass objectclass, objectHandleType handle)
{
	TRACE_ASSERT(objectclass < TRACE_NCLASSES, "prvTraceIsObjectExcluded: objectclass >= TRACE_NCLASSES", 1);
	TRACE_ASSERT(handle <= RecorderDataPtr->ObjectPropertyTable.NumberOfObjectsPerClass[objectclass], "uiTraceIsObjectExcluded: Invalid value for handle", 1);

	switch(objectclass)
	{
	case TRACE_CLASS_TASK:
		return TRACE_GET_TASK_FLAG_ISEXCLUDED(handle);
	case TRACE_CLASS_QUEUE:
		return TRACE_GET_QUEUE_FLAG_ISEXCLUDED(handle);
	}

	/* Must never reach */
	return 1;
}

/********************* Function wrappers that require KERNEL_SOURCE_FILE defined ***********************/

extern portBASE_TYPE xPortRaisePrivilege( void );
#define prvRESET_PRIVILEGE( xRunningPrivileged ) if( ( portBASE_TYPE )( xRunningPrivileged ) != pdTRUE ) { __asm( "	SVC	5	" ); }

void vTraceTaskInstanceIsFinished_MPU()
{
	portBASE_TYPE xRunningPrivileged;

	xRunningPrivileged = xPortRaisePrivilege();
	vTraceTaskInstanceIsFinished();
	prvRESET_PRIVILEGE( xRunningPrivileged );
}

extern void vTracePrintF_Helper(traceLabel eventLabel, const char* formatStr, va_list vl);
extern void vTraceChannelPrintF_Helper(UserEventChannel channel, va_list vl);

traceLabel xTraceOpenLabel_MPU(const char* label)
{
	traceLabel lbl;
	portBASE_TYPE xRunningPrivileged;

	xRunningPrivileged = xPortRaisePrivilege();
	lbl = xTraceOpenLabel(label);
	prvRESET_PRIVILEGE( xRunningPrivileged );
	return lbl;
}

void vTraceUserEvent_MPU(traceLabel eventLabel)
{
	portBASE_TYPE xRunningPrivileged;

	xRunningPrivileged = xPortRaisePrivilege();
	vTraceUserEvent(eventLabel);
	prvRESET_PRIVILEGE( xRunningPrivileged );
}

void vTracePrintF_MPU(traceLabel eventLabel, const char* formatStr, ...)
{
	va_list vl;
	portBASE_TYPE xRunningPrivileged;

	va_start(vl, formatStr);
	xRunningPrivileged = xPortRaisePrivilege();
	vTracePrintF_Helper(eventLabel, formatStr, vl);
	prvRESET_PRIVILEGE( xRunningPrivileged );
	va_end(vl);
}

#if (USE_SEPARATE_USER_EVENT_BUFFER == 1)
UserEventChannel xTraceRegisterChannelFormat_MPU(traceLabel channel, traceLabel formatStr)
{
	UserEventChannel uec;
	portBASE_TYPE xRunningPrivileged;

	xRunningPrivileged = xPortRaisePrivilege();
	uec = xTraceRegisterChannelFormat(channel, formatStr);
	prvRESET_PRIVILEGE( xRunningPrivileged );
	return uec;
}

void vTraceChannelPrintF_MPU(UserEventChannel channel, ...)
{
	va_list vl;
	portBASE_TYPE xRunningPrivileged;

	va_start(vl, formatStr);
	xRunningPrivileged = xPortRaisePrivilege();
	vTraceChannelPrintF_Helper(channel, vl);
	prvRESET_PRIVILEGE( xRunningPrivileged );
	va_end(vl);
}

void vTraceChannelUserEvent_MPU(UserEventChannel channel)
{
	portBASE_TYPE xRunningPrivileged;

	xRunningPrivileged = xPortRaisePrivilege();
	vTraceChannelUserEvent(channel);
	prvRESET_PRIVILEGE( xRunningPrivileged );
}

#endif

#endif