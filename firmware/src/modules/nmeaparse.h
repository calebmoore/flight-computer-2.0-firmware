/*
 * nmeaparse.h
 *
 * Created: 2/13/2015 5:22:04 PM
 *  Author: Caleb
 */ 


#ifndef NMEAPARSE_H_
#define NMEAPARSE_H_

#include "gps.h"

void nmea_init(void);
bool nmea_test(void);

bool nmea_is_data_ready(void);

int nmea_fuse(char c);
void nmea_reset(void);

bool nmea_get_motion(struct gps_motion *motion);
bool nmea_get_position(struct gps_position *position);
bool nmea_get_statistics(struct gps_statistics *stats);
bool nmea_get_time(struct gps_time *time);
bool nmea_get_lock_info(struct gps_lock_info *lock_info);


#endif /* NMEAPARSE_H_ */