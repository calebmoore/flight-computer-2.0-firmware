/*
 * log.c
 *
 * Created: 2/23/2015 2:23:33 PM
 *  Author: Caleb
 */ 

#include <asf.h>

#include <string.h>
#include <errno.h>
 
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"

#include "eprintf.h"
#include "uart.h"
#include "debug.h"

#include "config.h"
#include "telemetry.h"
#include "worker.h"

//#include "console.h"
#include "fwassert.h"

#include "log.h"

static const uint8_t _type_length[] = {
    [LOG_UINT8]  = 1,
    [LOG_UINT16] = 2,
    [LOG_UINT32] = 4,
    [LOG_INT8]   = 1,
    [LOG_INT16]  = 2,
    [LOG_INT32]  = 4,
    [LOG_FLOAT]  = 4,
//    [LOG_FP16]   = 2,
};

#define LOG_MAX_LENGTH (TLM_MAX_DATA_SIZE - 4)

#define LOG_MAX_OPS    64
#define LOG_MAX_BLOCKS 8
struct log_ops {
    struct log_ops *next;
    uint8_t storage_type : 4;
    uint8_t log_type     : 4;
    void *var;
};

struct log_block {
    int id;
    xTimerHandle timer;
    struct log_ops *ops;
};

static struct log_ops _log_ops[LOG_MAX_OPS];
static struct log_block _log_blocks[LOG_MAX_BLOCKS];
static xSemaphoreHandle _log_lock;

struct ops_setting {
    uint8_t log_type;
    uint8_t id;
} __attribute__((packed));

#define TABLE_OF_CONTENTS_CH 0
#define CONTROL_CH           1
#define LOG_CH               2

#define CMD_GET_ITEM         0
#define CMD_GET_INFO         1

#define CONTROL_CREATE_BLOCK 0
#define CONTROL_APPEND_BLOCK 1
#define CONTROL_DELETE_BLOCK 2
#define CONTROL_START_BLOCK  3
#define CONTROL_STOP_BLOCK   4
#define CONTROL_RESET        5

#define BLOCK_ID_FREE        -1

static bool _is_init = false;

static TelemetryPacket _packet;

// private utility functions
static void log_task(void *param);
static void log_table_of_contents_process(int command);
static void log_control_process(void);

static void log_run_block(void *arg);
static void log_block_timed(xTimerHandle timer);

// set by the linker
extern struct log_var _log_start;
extern struct log_var _log_stop;

// pointer to logging list with length
static struct log_var *_logs;
static int _logs_len;
static uint32_t _logs_crc;
static int _logs_count = 0;

// log management functions
static int log_append_block(int id, struct ops_setting *settings, int len);
static int log_create_block(unsigned char id, struct ops_setting *settings, int len);
static int log_delete_block(int id);
static int log_start_block(int id, unsigned int period);
static int log_stop_block(int id);
static void log_reset(void);

static void default_blocks_init(void);

/**
 *  
 */
void log_init(void) {
    int i;

    if (_is_init) {
        return;
    }

    _logs = &_log_start;
    _logs_len = &_log_stop - &_log_start;
    // calculate CRC of logs
    while(crc32_calculate(_logs, (size_t)_logs_len, (crc32_t *)&_logs_crc) != STATUS_OK);

    // lock that protects the log data structures
    _log_lock = xSemaphoreCreateMutex();

    for (i = 0; i < _logs_len; i++) {
        if (!(_logs[i].type & LOG_GROUP)) {
            _logs_count++;
        }
    }

    // maually free all log blocks
    for (i = 0; i < LOG_MAX_BLOCKS; i++) {
        _log_blocks[i].id = BLOCK_ID_FREE;
    }

    // init data structures and set log system to known state
    log_reset();

    // set up default logging
    default_blocks_init();

    xTaskCreate(log_task, (const char * const)TASK_NAME_LOG, 
            TASK_STACK_SIZE_LOG, NULL, TASK_PRIORITY_LOG, NULL);

    _is_init = true;
}


/**
 *  
 */
bool log_test(void) {
    return _is_init;
}


void log_task(void *param) {
    telemetry_init_task_queue(TLM_PORT_LOG);

    while (1) {
        telemetry_receive_packet_block(TLM_PORT_LOG, &_packet);

        xSemaphoreTake(_log_lock, portMAX_DELAY);
        if (_packet.channel == TABLE_OF_CONTENTS_CH) {
            log_table_of_contents_process(_packet.data[0]);
        } else if (_packet.channel == CONTROL_CH) {
            log_control_process();
        }
        xSemaphoreGive(_log_lock);
    }
}


void log_table_of_contents_process(int command) {
    int ptr = 0;
    const char *group = "";
    int n = 0;

    switch(command) {
    case CMD_GET_INFO: 
        // get info packet about log implementation
        DEBUG_PRINT("Packet is TOC_GET_INFO\n");
        ptr = 0;
        group = "";
        _packet.header = TLM_HEADER(TLM_PORT_LOG, TABLE_OF_CONTENTS_CH);
        _packet.size = 8;
        _packet.data[0] = CMD_GET_INFO;
        _packet.data[1] = _logs_count;
        memcpy(&_packet.data[2], &_logs_crc, 4);
        _packet.data[6] = LOG_MAX_BLOCKS;
        _packet.data[7] = LOG_MAX_OPS;

        telemetry_send_packet(&_packet);
        break;

    case CMD_GET_ITEM:
        // get log variable
        DEBUG_PRINT("Packet is TOC_GET_ITEM ID: %d\n", _packet.data[1]);
        for (ptr = 0; ptr < _logs_len; ptr++) {
            if (_logs[ptr].type & LOG_GROUP) {
                if (_logs[ptr].type & LOG_START) {
                    group = _logs[ptr].name;
                } else {
                    group = "";
                }
            } else {
                if (n == _packet.data[1]) {
                    break;
                }
                n++;
            }
        }

        if (ptr < _logs_len) {
            DEBUG_PRINT("    Item is \"%s\":\"%s\"\n", group, _logs[ptr].name);

            _packet.header = TLM_HEADER(TLM_PORT_LOG, TABLE_OF_CONTENTS_CH);
            _packet.data[0] = CMD_GET_ITEM;
            _packet.data[1] = n;
            _packet.data[2] = _logs[ptr].type;
            memcpy(_packet.data + 3, group, strlen(group) + 1);
            memcpy(_packet.data + 3 + strlen(group) + 1, _logs[ptr].name, strlen(_logs[ptr].name) + 1);
            _packet.size = (3 + 2 + strlen(group) + strlen(_logs[ptr].name));

            telemetry_send_packet(&_packet);
        } else {
            DEBUG_PRINT("    Index out of range!");

            _packet.header = TLM_HEADER(TLM_PORT_LOG, TABLE_OF_CONTENTS_CH);
            _packet.data[0] = CMD_GET_ITEM;
            _packet.size = 1;

            telemetry_send_packet(&_packet);
        }
        break;
    }
}


void log_control_process() {
    int ret = ENOEXEC;

    switch (_packet.data[0]) {
    case CONTROL_CREATE_BLOCK:
        ret = log_create_block(
                _packet.data[1], 
                (struct ops_setting *)&_packet.data[2],
                (_packet.size - 2) / sizeof(struct ops_setting)
        );
        break;

    case CONTROL_APPEND_BLOCK:
        ret = log_append_block(
                _packet.data[1],
                (struct ops_setting *)&_packet.data[2],
                (_packet.size - 2) / sizeof(struct ops_setting)
        );
        break;
    
    case CONTROL_DELETE_BLOCK:
        ret = log_delete_block(_packet.data[1]);
        break;

    case CONTROL_START_BLOCK:
        ret = log_start_block(_packet.data[1], _packet.data[2] * 10);
        break;

    case CONTROL_STOP_BLOCK:
        ret = log_stop_block(_packet.data[1]);
        break;

    case CONTROL_RESET:
        log_reset();
        ret = 0;
        break;
    }

    _packet.data[2] = ret;
    _packet.size = 3;

    telemetry_send_packet(&_packet);
}


static int log_create_block(unsigned char id, struct ops_setting *settings, int len) {
    int i;

    // check if block already exits
    for (i = 0; i < LOG_MAX_BLOCKS; i++) {
        if (id == _log_blocks[i].id) {
            return EEXIST;
        }
    }

    // find next free block
    for (i = 0; i < LOG_MAX_BLOCKS; i++) {
        if (_log_blocks[i].id == BLOCK_ID_FREE) {
            break;
        }
    }

    // check for full memory
    if (i == LOG_MAX_BLOCKS) {
        // out of memory
        return ENOMEM;
    }

    _log_blocks[i].id = id;
    _log_blocks[i].timer = xTimerCreate(
            (const char *)"log_timer", M2T(1000), pdTRUE, &_log_blocks[i], 
            log_block_timed
    );
    _log_blocks[i].ops = NULL;

    if (_log_blocks[i].timer == NULL) {
        _log_blocks[i].id = BLOCK_ID_FREE;
        return ENOMEM;
    }

    DEBUG_PRINT("Added block ID %d\n", id);

    return log_append_block(id, settings, len);
}


static int block_calc_length(struct log_block *block);
static struct log_ops *ops_malloc(void);
static void ops_free(struct log_ops *ops);
static void block_append_ops(struct log_block *block, struct log_ops *ops);
static int variable_get_index(int id);

static int log_append_block(int id, struct ops_setting *settings, int len) {
    int i;
    struct log_block *block;

    DEBUG_PRINT("Appending %d variable to block %d\n", len, id);

    // find block
    for (i = 0; i < LOG_MAX_BLOCKS; i++) {
        if (_log_blocks[i].id == id) {
            break;
        }
    }

    if (i >= LOG_MAX_BLOCKS) {
        DEBUG_PRINT("ERROR: Trying to append block id %d that doesn't exist.", id);
        return ENOENT;
    }

    block = &_log_blocks[i];

    for (i = 0; i < len; i++) {
        int current_len = block_calc_length(block);
        struct log_ops *ops;
        int var_id;

        if ((current_len + _type_length[settings[i].log_type & 0x0F]) > LOG_MAX_LENGTH) {
            DEBUG_PRINT("ERROR: Trying to append to a full block (id %d)\n", id);
            return E2BIG;
        }

        ops = ops_malloc();

        if (!ops) {
            DEBUG_PRINT("ERROR: No more ops memory free!\n");
            return ENOMEM;
        }

        if (settings[i].id != 255) {
            var_id = variable_get_index(settings[i].id);

            if (var_id < 0) {
                DEBUG_PRINT("ERROR: Trying to add variable ID %d that does not exist\n", settings[i].id);
                return ENOENT;
            }

            ops->var          = _logs[var_id].address;
            ops->storage_type = _logs[var_id].type;
            ops->log_type     = settings[i].log_type & 0x0F;

            DEBUG_PRINT("Appended variable %d to block %d\n", settings[i].id, id);
        } else {
            // check that address exists in RAM
            ops->var          = (void *)(&settings[i] + 1);
            ops->storage_type = (settings[i].log_type >> 4) & 0x0F;
            ops->log_type     = settings[i].log_type & 0x0F;
            i += 2;

            DEBUG_PRINT("Appended var address 0x%X to block %d\n", (int)ops->var, id);
        }

        block_append_ops(block, ops);

        DEBUG_PRINT("    Block now length %d\n", block_calc_length(block));
    }

    return 0;
}


static int log_delete_block(int id) {
    int i;
    struct log_ops *ops;
    struct log_ops *ops_next;

    // find block
    for (i = 0; i < LOG_MAX_BLOCKS; i++) {
        if (_log_blocks[i].id == id) {
            break;
        }
    }

    if (i >= LOG_MAX_BLOCKS) {
        DEBUG_PRINT("ERROR: Trying to delete block that doesn't exist (id %d)\n", id);
        return ENOENT;
    }

    // delete ops
    ops = _log_blocks[i].ops;
    while (ops) {
        ops_next = ops->next;
        ops_free(ops);
        ops = ops_next;
    }

    // free FreeRTOS timers
    if (_log_blocks[i].timer != 0) {
        xTimerStop(_log_blocks[i].timer, portMAX_DELAY);
        xTimerDelete(_log_blocks[i].timer, portMAX_DELAY);
        _log_blocks[i].timer = 0;
    }

    _log_blocks[i].id = BLOCK_ID_FREE;
    
    return 0;
}


static int log_start_block(int id, unsigned int period) {
    int i;
    
    // find block
    for (i = 0; i < LOG_MAX_BLOCKS; i++) {
        if (_log_blocks[i].id == id) {
            break;
        }
    }

    if (i >= LOG_MAX_BLOCKS) {
        DEBUG_PRINT("ERROR: Trying to start block that doesn't exist (id %d)\n", id);
        return ENOENT;
    }

    DEBUG_PRINT("Starting block %d with period %d ms\n", id, period);

    if (period > 0) {
        xTimerChangePeriod(_log_blocks[i].timer, M2T(period), 100);
        xTimerStart(_log_blocks[i].timer, 100);
    } else {
        // single shot log
        worker_schedule_job(log_run_block, &_log_blocks[i]);
    }

    return 0;
}


static int log_stop_block(int id) {
    int i;

    // find block
    for (i = 0; i < LOG_MAX_BLOCKS; i++) {
        if (_log_blocks[i].id == id) {
            break;
        }
    }

    if (i >= LOG_MAX_BLOCKS) {
        DEBUG_PRINT("ERROR: Trying to stop block that doesn't exist (id %d)\n", id);
        return ENOENT;
    }

    xTimerStop(_log_blocks[i].timer, portMAX_DELAY);

    return 0;
}


static void log_block_timed(xTimerHandle timer) {
    worker_schedule_job(log_run_block, pvTimerGetTimerID(timer));
}


static void log_run_block(void *arg) {
    struct log_block *block = arg;
    struct log_ops *ops = block->ops;
    static TelemetryPacket p;
    unsigned int timestamp;

    xSemaphoreTake(_log_lock, portMAX_DELAY);

    timestamp = ((long)xTaskGetTickCount())/portTICK_RATE_MS;

    p.header = TLM_HEADER(TLM_PORT_LOG, LOG_CH);
    p.size = 4;
    p.data[0] = block->id;
    p.data[1] = (timestamp >>  0) & 0x00FF;
    p.data[2] = (timestamp >>  8) & 0x00FF;
    p.data[3] = (timestamp >> 16) & 0x00FF;

    while (ops) {
        int value_i = 0;
        float value_f = 0;

        switch(ops->storage_type) {
        case LOG_UINT8:
            value_i = *(uint8_t *)ops->var;
            break;

        case LOG_UINT16:
            value_i = *(uint16_t *)ops->var;
            break;

        case LOG_UINT32:
            value_i = *(uint32_t *)ops->var;
            break;
        
        case LOG_INT8:
            value_i = *(int8_t *)ops->var;
            break;

        case LOG_INT16:
            value_i = *(int16_t *)ops->var;
            break;
        
        case LOG_INT32:
            value_i = *(int32_t *)ops->var;
            break;

        case LOG_FLOAT:
            value_i = *(float *)ops->var;
            break;
        }

        if (ops->log_type == LOG_FLOAT) {
            // log type is float
            value_f = *(float *)ops->var;

            // add to packet
            memcpy(&p.data[p.size], &value_f, 4);
            p.size += 4;
        } else {
            // log type is integer
            memcpy(&p.data[p.size], &value_i, _type_length[ops->log_type]);
            p.size += _type_length[ops->log_type];
        }

        ops = ops->next;
    }

    xSemaphoreGive(_log_lock);

    // TODO: check if connection is still up
    if (true) {
        telemetry_send_packet(&p);
    } else {
        // downlink offline
        log_reset();
        telemetry_reset();
    }
}


static int variable_get_index(int id) {
    int i;
    int n = 0;

    // look for index
    for (i = 0; i < _logs_len; i++) {
        if (!(_logs[i].type & LOG_GROUP)) {
            if (n == id) {
                break;
            }
            n++;
        }
    }

    // check if found
    if (i >= _logs_len) {
        // not found
        return -1;
    }

    return i;
}

static struct log_ops *ops_malloc() {
    int i;

    // find free space
    for (i = 0; i < LOG_MAX_OPS; i++) {
        if (_log_ops[i].var == NULL) {
            break;
        }
    }

    // check if found
    if (i >= LOG_MAX_OPS) {
        // not found
        return NULL;
    }

    // return address of allocated space
    return &_log_ops[i];
}

static void ops_free(struct log_ops *ops) {
    ops->var = NULL;
}

static int block_calc_length(struct log_block *block) {
    struct log_ops *ops;
    int len = 0;

    for (ops = block->ops; ops; ops = ops->next) {
        len += _type_length[ops->log_type];
    }

    return len;
}

void block_append_ops(struct log_block  *block, struct log_ops *ops) {
    struct log_ops *o;

    ops->next = NULL;

    if (block->ops == NULL) {
        block->ops = ops;
    } else {
        for (o = block->ops; o->next; o = o->next);
        o->next = ops;
    }
}

static void log_reset() {
    int i;
    
    if (_is_init) {
        // stop and delete everything
        for (i = 0; i < LOG_MAX_BLOCKS; i++) {
            if (_log_blocks[i].id != -1) {
                log_stop_block(_log_blocks[i].id);
                log_delete_block(_log_blocks[i].id);
            }
        }
    }

    // force free every block object
    for (i = 0; i < LOG_MAX_BLOCKS; i++) {
        _log_blocks[i].id = BLOCK_ID_FREE;
    }

    // force free every log op
    for (i = 0; i < LOG_MAX_OPS; i++) {
        _log_ops[i].var = NULL;
    }
}


/**
 * Public API used to access log table of contents from within the firmware. 
 */
int log_get_var_id(const char *group, const char *name) {
    int i;
    int n = 0;
    const char *current_group = "";

    for (i = 0; i < _logs_len; i++) {
        if (_logs[i].type & LOG_GROUP) {
            if (_logs[i].type & LOG_START) {
                current_group = _logs[i].name;
            }
            continue;
        }

        if ((!strcmp(group, current_group)) && (!strcmp(name, _logs[i].name))) {
            return n;
        }

        n++;
    }

    return -1;
}

int log_get_int(int var_id) {
    int value_i = 0;

    ASSERT(var_id >= 0);

    switch(_logs[var_id].type) {
    case LOG_UINT8:
        value_i = *(uint8_t *)_logs[var_id].address;
        break;
    
    case LOG_UINT16:
        value_i = *(uint16_t *)_logs[var_id].address;
        break;

    case LOG_UINT32:
        value_i = *(uint32_t *)_logs[var_id].address;
        break;

    case LOG_INT8:
        value_i = *(int8_t *)_logs[var_id].address;
        break;

    case LOG_INT16:
        value_i = *(int16_t *)_logs[var_id].address;
        break;

    case LOG_INT32:
        value_i = *(int32_t *)_logs[var_id].address;
        break;

    case LOG_FLOAT:
        value_i = *(float *)_logs[var_id].address;
        break;
    }

    return value_i;
}

float log_get_float(int var_id) {
    ASSERT(var_id >= 0);

    if (_logs[var_id].type == LOG_FLOAT) {
        return *(float *)_logs[var_id].address;
    }

    // not actually a float, return the int
    return log_get_int(var_id);
}

unsigned int log_get_uint(int var_id) {
    return (unsigned int)log_get_int(var_id);
}

static void default_blocks_init() {
    struct ops_setting gps_stats[] = {
        { .id = log_get_var_id("gps_stats", "data_good"), .log_type = LOG_UINT8 },
        { .id = log_get_var_id("gps_stats", "chars"), .log_type = LOG_UINT32 },
        { .id = log_get_var_id("gps_stats", "sent"), .log_type = LOG_UINT32 },
        { .id = log_get_var_id("gps_stats", "chkfail"), .log_type = LOG_UINT32 },
    };

    struct ops_setting gps_data[] = {
        { .id = log_get_var_id("gps_pos", "lat"), .log_type = LOG_FLOAT },
        { .id = log_get_var_id("gps_pos", "lon"), .log_type = LOG_FLOAT },
        { .id = log_get_var_id("gps_pos", "alt"), .log_type = LOG_FLOAT },      
        { .id = log_get_var_id("gps_motion", "speed"), .log_type = LOG_FLOAT },
        { .id = log_get_var_id("gps_motion", "course"), .log_type = LOG_FLOAT },
        { .id = log_get_var_id("gps_link", "hdop"), .log_type = LOG_UINT8 },
        { .id = log_get_var_id("gps_link", "sats"), .log_type = LOG_UINT8 },
    };

    log_create_block(0, gps_stats, 4);
    log_create_block(1, gps_data, 7);

    log_start_block(0, 500);
    log_start_block(1, 1000);
}