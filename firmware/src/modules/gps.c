/*
 * gps.c
 *
 * Created: 2/11/2015 8:54:24 PM
 *  Author: Winglee Plasma Lab
 */ 

#include <asf.h>

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"

#include "config.h"
#include "statseq.h"

#include "nmeaparse.h"
#include "gps.h"

static bool _is_init = false;
static volatile bool _gps_is_enabled = false;

static xSemaphoreHandle _rx_complete_semphr;

static struct usart_module gps_usart;

// private functions
static void configure_enable_pin(void);
static void configure_usart(struct usart_module *instance);
static void gps_tx_callback(void); // TODO: GPS TX
static void gps_rx_callback(const struct usart_module *const usart_module);

/**
 * FreeRTOS task.
 */
static void gps_task(void *arg);

/**
 * Initializes and creates GPS task.
 */
void gps_init() {	
	if (_is_init) {
		return;
	}
	
	// start NMEA parser
	nmea_init();	
	
	xTaskCreate(
			gps_task, 
			(const char *const)TASK_NAME_GPS,
			TASK_STACK_SIZE_GPS, 
			NULL, 
			TASK_PRIORITY_GPS, 
			NULL
	);
    
    _is_init = true;
}

/**
 *	Tests GPS module.
 */
bool gps_test() {
    return _is_init;
}

/**
 * Applies power to GPS unit and enables USART reception. 
 */
void gps_enable() {
	if (_gps_is_enabled) {
		return;
	}
	
	_gps_is_enabled = true;
	
	usart_enable_callback(&gps_usart, USART_CALLBACK_BUFFER_RECEIVED);
	port_pin_set_output_level(GPS_ENABLE_PIN, true);
}

/**
 * Removes power from the GPS unit and disables USART reception. 
 */
void gps_disable() {
	if (!_gps_is_enabled) {
		return;
	}
	
	_gps_is_enabled = false;
	
	port_pin_set_output_level(GPS_ENABLE_PIN, false);
	usart_disable_callback(&gps_usart, USART_CALLBACK_BUFFER_RECEIVED);
}

/**
 * Whether the GPS unit has power.
 *
 * @return true if powered, else false 
 */
bool gps_is_enabled() {
    return _gps_is_enabled;
}

bool gps_get_motion(struct gps_motion *motion) {
    return nmea_get_motion(motion);
}

bool gps_get_position(struct gps_position *position) {
    return nmea_get_position(position);
}

bool gps_get_statistics(struct gps_statistics *stats) {
    return nmea_get_statistics(stats);
}

bool gps_get_time(struct gps_time *time) {
    return nmea_get_time(time);
}

bool gps_get_lock_info(struct gps_lock_info *lock_info) {
    return nmea_get_lock_info(lock_info);
}

/**
 * Receives characters from the GPS' USART channel, and feeds them into the 
 * NMEA parser. 
 */
static void gps_task(void *arg) {	
	char rx_buffer;
	
	// create and take rx semaphore
	vSemaphoreCreateBinary(_rx_complete_semphr);
	configASSERT(_rx_complete_semphr);
	
	xSemaphoreTake(_rx_complete_semphr, 0);
	
	// configure and enable gps communication
	configure_usart(&gps_usart);
	configure_enable_pin();
	
	// turn on the gps
	gps_enable();
	
	while (1) {
		// no sense wasting CPU if the module is known to be off
		if (!_gps_is_enabled) {
			continue;
		}
		
		// receive character from GPS, give to NMEA parser
		usart_read_buffer_job(&gps_usart, (uint8_t *)&rx_buffer, sizeof(rx_buffer));
		if (xSemaphoreTake(_rx_complete_semphr, portMAX_DELAY) == pdPASS) {
			// blinkenlights
            statseq_run(LED_STATUS_1, seq_blip);

            // give to NMEA parser
			nmea_fuse(rx_buffer);	
		}
	}
}


/**
 * Configures and enables the USART used to communicate with the GPS. 
 */
static void configure_usart(struct usart_module *instance) {
	struct usart_config gps_usart_config;
	
	usart_get_config_defaults(&gps_usart_config);
	gps_usart_config.baudrate = GPS_BAUDRATE;
	gps_usart_config.mux_setting = GPS_SERCOM_MODE;
	gps_usart_config.pinmux_pad0 = GPS_SERCOM_PAD0_PINMUX;
	gps_usart_config.pinmux_pad1 = GPS_SERCOM_PAD1_PINMUX;
	gps_usart_config.pinmux_pad2 = GPS_SERCOM_PAD2_PINMUX;
	gps_usart_config.pinmux_pad3 = GPS_SERCOM_PAD3_PINMUX;
	
	// initialize the module
	while (usart_init(instance, GPS_SERCOM, &gps_usart_config) != STATUS_OK);
	
	usart_enable(instance);
	
	usart_register_callback(instance, gps_rx_callback, USART_CALLBACK_BUFFER_RECEIVED);
}

/**
 *  Configures the discrete output used to enable or disable the GPS unit. 
 */
static void configure_enable_pin() {
	struct port_config gps_enable_config;
	
	// initiate GPS enable pin
	port_get_config_defaults(&gps_enable_config);
	gps_enable_config.direction = PORT_PIN_DIR_OUTPUT;
	port_pin_set_config(GPS_ENABLE_PIN, &gps_enable_config);	
}

/**
 * GPS RX ISR.
 */
static void gps_rx_callback(const struct usart_module *const usart_module) {
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	
	(void)usart_module;
	
	xSemaphoreGiveFromISR(_rx_complete_semphr, &xHigherPriorityTaskWoken);

	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

/**
 *   
 */
static void gps_tx_callback() {
	// TODO: GPS PROGRAMMING
}

