/*
 * gps.h
 *
 * Created: 2/11/2015 8:54:39 PM
 *  Author: Winglee Plasma Lab
 */ 


#ifndef GPS_H_
#define GPS_H_

#include <asf.h>

#define GPS_BAUDRATE		   4800
#define GPS_ENABLE_PIN         PIN_PA24

// GPS SERCOM config
#define GPS_SERCOM             SERCOM3
#define GPS_SERCOM_MODE	       USART_RX_1_TX_0_XCK_1
#define GPS_SERCOM_PAD0_PINMUX PINMUX_PA22C_SERCOM3_PAD0
#define GPS_SERCOM_PAD1_PINMUX PINMUX_PA23C_SERCOM3_PAD1
#define GPS_SERCOM_PAD2_PINMUX PINMUX_UNUSED	
#define GPS_SERCOM_PAD3_PINMUX PINMUX_UNUSED

struct gps_time {
    uint8_t hour;
    uint8_t min;
    uint8_t sec;
    uint8_t day;
    uint8_t month;
    uint8_t year;
};

struct gps_position {
	float latitude;
	float longitude;
	float altitude;
};

struct gps_motion {
	float speed;
	float course;		
};

struct gps_lock_info {
	int32_t hdop;
	uint8_t satellites;
};

struct gps_statistics {
	uint32_t chars;
	uint32_t sentences;
	uint32_t chk_err;
};


/**
 * Initialize GPS module. 
 */
void gps_init(void);


/**
 * Test GPS module. 
 */
bool gps_test(void);


/**
 * Enables GPS data reception. 
 */
void gps_enable(void);


/**
 * Disables GPS data reception. 
 */
void gps_disable(void);


/**
 * Whether the GPS unit is enabled
 *
 * @return true if enabled, false otherwise 
 */
bool gps_is_enabled(void);

// TODO: this feels really redundant since these are simply wrappers for the 
// same functions in the NMEA parser
bool gps_get_motion(struct gps_motion *motion);
bool gps_get_position(struct gps_position *position);
bool gps_get_statistics(struct gps_statistics *stats);
bool gps_get_time(struct gps_time *time);
bool gps_get_lock_info(struct gps_lock_info *lock_info);

#endif /* GPS_H_ */