/*
 * log.h
 *
 * Created: 2/23/2015 2:23:24 PM
 *  Author: Caleb
 */ 


#ifndef LOG_H_
#define LOG_H_

void log_init(void);
bool log_test(void);

// variable retrieval
int log_get_var_id(const char *group, const char *name);
float log_get_float(int var_id);
int log_get_int(int var_id);
unsigned int log_get_uint(int var_id);

// basic log structure
struct log_var {
    uint8_t type;
    char *name;
    void *address;
};

#define LOG_UINT8  1
#define LOG_UINT16 2
#define LOG_UINT32 3
#define LOG_INT8   4
#define LOG_INT16  5
#define LOG_INT32  6
#define LOG_FLOAT  7

#define LOG_GROUP 0x80
#define LOG_START 1
#define LOG_STOP  0

#define LOG_ADD(TYPE, NAME, ADDRESS) \
    { .type = TYPE, .name = #NAME, .address = (void*)(ADDRESS), },

#define LOG_ADD_GROUP(TYPE, NAME, ADDRESS) \
    { \
    .type = TYPE, .name = #NAME, .address = (void*)(ADDRESS), },

#define LOG_GROUP_START(NAME)  \
    static const struct log_var __logs_##NAME[] __attribute__((section(".log." #NAME), used)) = { \
        LOG_ADD_GROUP(LOG_GROUP | LOG_START, NAME, 0x0)

#define LOG_GROUP_STOP(NAME) \
    LOG_ADD_GROUP(LOG_GROUP | LOG_STOP, stop_##NAME, 0x0) \
};

#endif /* LOG_H_ */