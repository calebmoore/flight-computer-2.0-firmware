/*
 * telemetryservice.h
 *
 * Created: 2/23/2015 1:08:08 PM
 *  Author: Caleb
 */ 


#ifndef TELEMETRYSERVICE_H_
#define TELEMETRYSERVICE_H_

void telemetryservice_init(void);

bool telemetryservice_test(void);

#endif /* TELEMETRYSERVICE_H_ */