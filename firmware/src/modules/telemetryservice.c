/*
 * telemetryservice.c
 *
 * Created: 2/23/2015 1:07:48 PM
 *  Author: Caleb
 */ 

 #include <asf.h>

 #include "FreeRTOS.h"
 #include "task.h"

 #include "telemetry.h"
 #include "telemetryservice.h"

 static bool _is_init = false;

 typedef enum {
    TLM_LINK_ECHO = 0x00,
    TLM_LINK_SRC  = 0x01,
    TLM_LINK_SINK = 0x02
 } TelemetryLinkNumber;

static void telemetryservice_handler(TelemetryPacket *p);

void telemetryservice_init() {
    if (_is_init) {
        return;
    }

    // register a callback to service the link port
    telemetry_register_port_callback(TLM_PORT_LINK, telemetryservice_handler);

    _is_init = true;
}

bool telemetryservice_test() {
    return _is_init;
}

static void telemetryservice_handler(TelemetryPacket *p) {
    switch(p->channel) {
    case TLM_LINK_ECHO:
        telemetry_send_packet(p);
        break;

    case TLM_LINK_SRC:
        p->size = TLM_MAX_DATA_SIZE;
        telemetry_send_packet(p);
        break;

    case TLM_LINK_SINK:
        // ignore the packet
        break;

    default:
        break;
    }
}