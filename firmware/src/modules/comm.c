/*
 * comm.c
 *
 * Created: 2/6/2015 2:42:26 PM
 *  Author: Winglee Plasma Lab
 */ 

#include <asf.h>

#include "telemetry.h"
#include "telemetryservice.h"
#include "log.h"
#include "gps.h"
#include "radiolink.h"

#include "comm.h"

static bool _is_init;


/**
 * Initializes each communication module.
 */
void comm_init() {
    if (_is_init) {
        return;
    }
    
    // comm link init
    radiolink_init();

    telemetry_init();
    telemetry_set_link(radiolink_get_link());

    telemetryservice_init();
    
    // init comm modules
    log_init();
    gps_init();
    // console
    // payload
    // flash
    // param system
    
    _is_init = true;
}


/**
 * Tests each communication module.
 */
bool comm_test() {
    bool pass = _is_init;
    
    pass &= radiolink_test();
    pass &= telemetry_test();
    pass &= telemetryservice_test();
    
    //pass &= console_test();
    pass &= log_test();
    pass &= gps_test();
    //pass &= payload_test();
    //pass &= param_test();
    
    return pass;
}