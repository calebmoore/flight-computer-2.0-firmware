/*
 * platform.c
 *
 * Created: 1/31/2015 11:54:22 AM
 *  Author: Caleb
 */ 

#include <asf.h>
#include <stdbool.h>

#include "platform.h"

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#include "version.h"
#include "debug.h"
#include "eprintf.h"
#include "uart.h"

#include "log.h"
#include "gps.h"
#include "config.h"
#include "comm.h"
#include "worker.h"
#include "statind.h"
#include "statseq.h"
#include "pyros.h"

static bool self_test_passed;
static bool can_fly;
static bool _is_init;

// for platform wide synchronization
xSemaphoreHandle can_start_mutex;

static void platform_task(void *arg);

/**
 *  Creates the platform RTOS task. 
 */
void platform_launch() {
	xTaskCreate(platform_task, (const char * const)TASK_NAME_PLATFORM,
			TASK_STACK_SIZE_PLATFORM, NULL, TASK_PRIORITY_PLATFORM, NULL);
}

/**
 *   
 */
void platform_init() {
	if (_is_init)
		return;
		
	can_start_mutex = xSemaphoreCreateMutex();
	xSemaphoreTake(can_start_mutex, portMAX_DELAY);
	
	// initialize modules
	
	worker_init();
    statseq_init();
    pyros_init();
	   
	_is_init = true;
}

/**
 *   
 */
bool platform_test() {
	bool pass = _is_init;
	
	// test initialized modules
	pass &= worker_test();
    pass &= statseq_test();
    pass &= pyros_test();
	
	return pass;
}

/**
 *   
 */
void platform_task(void *arg) {
	bool pass = true;
	
	statind_init();
	
    uart_init(); // debugging

	// init high level modules
	platform_init();
	comm_init();
	
    DEBUG_PRINT("\n\n+----------------+\n");
    DEBUG_PRINT("| Hello!         |\n");
    DEBUG_PRINT("| ~ Finster <3 ~ |\n");
    DEBUG_PRINT("+----------------+\n\n");
    DEBUG_PRINT("Firmware V%s.%s.%s (%s)\n", 
            V_MAJOR, V_MINOR, V_REV, 
            (V_MODIFIED) ? "MODIFIED" : "CLEAN");
    DEBUG_PRINT("FreeRTOS %s\n\n", tskKERNEL_VERSION_NUMBER);
    DEBUG_PRINT("Initializing platform...\n");

    // test modules
    pass &= platform_test();
	pass &= comm_test();
       
	if (pass) {
		self_test_passed = true;
		
		platform_start();
        statseq_run(LED_STATUS_2, seq_armed);
        statseq_run(LED_STATUS_2, seq_test_passed);
        //statseq_run(BUZZER, seq_test_passed);
        //statseq_run(BUZZER, seq_armed);    
	} else {
		self_test_passed = false;
		
		if (platform_test()) {
			while (1) {
                // something is wrong, alert user
                statseq_run(LED_STATUS_2, seq_error);
                //statseq_run(BUZZER, seq_error);
                vTaskDelay(M2T(2000));
			}
		} else {
			// something is REALLY wrong
            statind_init();
            statind_set(LED_STATUS_2, true);
		}
	}
	
    // up and running, all tests pass
    DEBUG_PRINT("Done. (Free heap: %d Bytes)\n\n", xPortGetFreeHeapSize());

	// TODO: worker/flight loop
    worker_loop();
    
    // should never get here
    while (1) {
        vTaskDelay(portMAX_DELAY);
    }
}

/**
 *   
 */
void platform_start() {
	xSemaphoreGive(can_start_mutex);
}

/**
 *   
 */
void platform_wait_for_start() {
	while(!_is_init)
		vTaskDelay(2);
		
	xSemaphoreTake(can_start_mutex, portMAX_DELAY);
	xSemaphoreGive(can_start_mutex);
}

/**
 *   
 */
void platform_set_ready(bool is_ready) {
	can_fly = is_ready;
}

/**
 *   
 */
bool platform_is_ready() {
	return can_fly;
}


void vApplicationIdleHook(void) {
    static uint32_t time_to_print = M2T(10000);

    if (xTaskGetTickCount() - time_to_print >= M2T(10000)) {
        time_to_print = xTaskGetTickCount();
        DEBUG_PRINT("SYS::MET %d ms\n", time_to_print);
    }
}


// logging
LOG_GROUP_START(platform)
LOG_ADD(LOG_INT8, can_fly, &can_fly)
LOG_GROUP_STOP(platform)