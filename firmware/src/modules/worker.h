/*
 * worker.h
 *
 * Created: 2/1/2015 1:38:11 PM
 *  Author: Caleb
 */ 


#ifndef WORKER_H_
#define WORKER_H_

#include <asf.h>

void worker_init(void);
bool worker_test(void);

void worker_loop(void);
int worker_schedule_job(void (*function)(void *), void *arg);
int worker_schedule_job_from_isr(void (*function)(void *), void *arg);


#endif /* WORKER_H_ */