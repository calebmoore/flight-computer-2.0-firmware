/*
 * comm.h
 *
 * Created: 2/6/2015 2:42:34 PM
 *  Author: Winglee Plasma Lab
 */ 


#ifndef COMM_H_
#define COMM_H_

void comm_init(void);
bool comm_test(void);

#endif /* COMM_H_ */