/*
 * telemetry.h
 *
 * Created: 2/4/2015 5:49:06 PM
 *  Author: Winglee Plasma Lab
 */ 


#ifndef TELEMETRY_H_
#define TELEMETRY_H_

#include <asf.h>

#define TLM_MAX_DATA_SIZE         (30)
#define TLM_HEADER(port, channel) (((port & 0x0F) << 4) | (channel & 0x0F))
#define TLM_PACKET_IS_NULL(p)     ((p.header & 0xF3) == 0xF3)

typedef enum {
    TLM_PORT_CONSOLE = 0x00,
    TLM_PORT_PARAM   = 0x02,
    TLM_PORT_COMMAND = 0x03,
    TLM_PORT_MEM     = 0x04,
    TLM_PORT_LOG     = 0x05,
    TLM_PORT_GPS     = 0x06,
    TLM_PORT_LINK    = 0x0F
} TelemetryPort;

COMPILER_PACK_SET();
typedef struct TelemetryPacket {
    uint8_t size;
    union {
        struct {
            union {
                uint8_t header;
                struct {
                    uint8_t channel :2;
                    uint8_t id      :2;
                    uint8_t port    :4;
                };
            };
            uint8_t data[TLM_MAX_DATA_SIZE];
        };
        uint8_t raw[TLM_MAX_DATA_SIZE + 1];
    };
} __attribute__((packed)) TelemetryPacket;

typedef void (*TelemetryCallback)(TelemetryPacket *);

void telemetry_init(void);
bool telemetry_test(void);

void telemetry_init_task_queue(TelemetryPort task_id);

void telemetry_register_port_callback(int port, TelemetryCallback cb);

int telemetry_send_packet(TelemetryPacket *p);

int telemetry_send_packet_block(TelemetryPacket *p);

int telemetry_receive_packet(TelemetryPort task_id, TelemetryPacket *p);

int telemetry_receive_packet_wait(
        TelemetryPort task_id, 
        TelemetryPacket *p, 
        int wait
);

int telemetry_get_free_tx_queue_packets(void);

int telemetry_receive_packet_block(TelemetryPort task_id, TelemetryPacket *p);

void telemetry_packet_received(TelemetryPacket *p);

struct telemetry_link_operations {
    int (*set_enable)(bool enable);
    int (*send_packet)(TelemetryPacket *p);
    int (*receive_packet)(TelemetryPacket *p);
    int (*is_connected)(void);
    int (*reset)(void);
};

void telemetry_set_link(struct telemetry_link_operations *new_link);

bool telemetry_is_connected(void);

int telemetry_reset(void);

#endif /* TELEMETRY_H_ */