/*
 * platform.h
 *
 * Created: 1/31/2015 11:54:05 AM
 *  Author: Caleb
 */ 


#ifndef PLATFORM_H_
#define PLATFORM_H_

#include <asf.h>

/**
 *  Initializes the platform module. 
 */
void platform_init(void);

/**
 *  Self-test of the platform module. 
 */
bool platform_test(void);

/**
 *  Initializes dependencies and prepares the platform for liftoff. 
 */
void platform_launch(void);

/**
 *  Starts platform execution.
 */
void platform_start(void);

/**
 *  Halts current task until platform is ready to fly.
 */
void platform_wait_for_start(void);

/**
 *  Sets whether the platform is ready to fly.
 */
void platform_set_ready(bool is_ready);

/**
 *  Whether the platform ready to fly. 
 */
bool platform_is_ready(void);

#endif /* platform_H_ */