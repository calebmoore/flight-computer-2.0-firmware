/*
 * telemetry.c
 *
 * Created: 2/4/2015 5:49:21 PM
 *  Author: Winglee Plasma Lab
 */ 

#include <asf.h>
#include <errno.h>

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"

#include "eprintf.h"
#include "uart.h"
#include "debug.h"

#include "config.h"
#include "fwassert.h"
#include "statind.h"

#include "telemetry.h"

#define TLM_N_PORTS 16
#define TLM_TX_QUEUE_SIZE 60
#define TLM_RX_QUEUE_SIZE 2

static bool _is_init;
static int nop_function(void);

/**
 * Placeholder telemetry link. Performs no functions.
 */
static struct telemetry_link_operations nop_link = {
    .set_enable = (void*) nop_function,
    .send_packet = (void*) nop_function,
    .receive_packet = (void*) nop_function
};

static struct telemetry_link_operations *_link = &nop_link; // telemetry link

// main packet queues
static xQueueHandle _tx_queue;  
static xQueueHandle _rx_queue; 

// task packet queues
static xQueueHandle _queues[TLM_N_PORTS];
static volatile TelemetryCallback _callbacks[TLM_N_PORTS];

// RTOS tasks
static void telemetry_tx_task(void *param);
static void telemetry_rx_task(void *param);


/**
 * Initiates packet queues and RTOS tasks. 
 */
void telemetry_init() {
    if (_is_init)
        return;
        
    _tx_queue = xQueueCreate(TLM_TX_QUEUE_SIZE, sizeof(TelemetryPacket));
    _rx_queue = xQueueCreate(TLM_RX_QUEUE_SIZE, sizeof(TelemetryPacket));
    
    xTaskCreate(telemetry_tx_task, (const char *const)TASK_NAME_TLM_TX, 
            TASK_STACK_SIZE_TLM_TX, NULL, TASK_PRIORITY_TLM_TX, NULL);
    xTaskCreate(telemetry_rx_task, (const char *const)TASK_NAME_TLM_RX,
            TASK_STACK_SIZE_TLM_RX, NULL, TASK_PRIORITY_TLM_RX, NULL);
            
    _is_init = true;
}


/**
 *  
 */
bool telemetry_test() {
    return _is_init;
}


/**
 *  
 */
void telemetry_init_task_queue(TelemetryPort task_id) {
    ASSERT(_queues[task_id] == NULL);
    
    _queues[task_id] = xQueueCreate(1, sizeof(TelemetryPacket));
}


/**
 * Attempts a packet reception.
 */
int telemetry_receive_packet(TelemetryPort task_id, TelemetryPacket *p) {
    return telemetry_receive_packet_wait(task_id, p, 0);
}


/**
 * Blocks until packet is received.
 */
int telemetry_receive_packet_block(TelemetryPort task_id, TelemetryPacket *p) {
    return telemetry_receive_packet_wait(task_id, p, portMAX_DELAY);
}


/**
 *  
 */
int telemetry_receive_packet_wait(TelemetryPort task_id, TelemetryPacket *p, int wait) {
    ASSERT(_queues[task_id]);
    ASSERT(p);
    
    return xQueueReceive(_queues[task_id], p, portMAX_DELAY);
}


/**
 *  
 */
int telemetry_get_free_tx_queue_packets() {
    return (TLM_TX_QUEUE_SIZE - uxQueueMessagesWaiting(_tx_queue));
}


/**
 *  
 */
void telemetry_register_port_callback(int port, TelemetryCallback cb) {
    if (port > TLM_N_PORTS) {
        return;
    }
    
    _callbacks[port] = cb;
} 


/**
 *  
 */
int telemetry_send_packet(TelemetryPacket *p) {
    ASSERT(p);
    
    return xQueueSend(_tx_queue, p, 0);
}


/**
 *  
 */
int telemetry_send_packet_block(TelemetryPacket *p) {
    ASSERT(p);
    
    return xQueueSend(_tx_queue, p, portMAX_DELAY);
}


/**
 *  
 */
int telemetry_reset(void) {
    xQueueReset(_tx_queue);
    if (_link->reset) {
        _link->reset();
    }
    
    return 0;
}


/**
 *  
 */
bool telemetry_is_connected() {
    if (_link->is_connected) {
        return _link->is_connected();
    }        
    
    return true;
}


/**
 *  
 */
void telemetry_packet_received(TelemetryPacket *p) {
    portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
    xQueueSendFromISR(_rx_queue, p, &xHigherPriorityTaskWoken);
}


/**
 *  
 */
void telemetry_set_link(struct telemetry_link_operations *new_link) {
    if (_link) {
        _link->set_enable(false);
    }
    
    if (new_link) {
        _link = new_link;
    } else {
        _link = &nop_link;
    }
    
    _link->set_enable(true);
}

/**
 *  
 */
static void telemetry_tx_task(void *param) {
    TelemetryPacket p;
    
    while (true) {
        if (xQueueReceive(_tx_queue, &p, portMAX_DELAY) == pdTRUE) {
            while (_link->send_packet(&p) == false);
        }
    }
}


/**
 *  
 */
static void telemetry_rx_task(void *param) {
    TelemetryPacket p;
    static unsigned int dropped_packets = 0;
    
    while (1) {
        if (!_link->receive_packet(&p)) {
            DEBUG_PRINT("Packet routed.\n");
            if (_queues[p.port]) {
                xQueueSend(_queues[p.port], &p, 0);
            } else {
                DEBUG_PRINT("Packet dropped.\n");
                dropped_packets++;
            }
            
            if (_callbacks[p.port]) {
                DEBUG_PRINT("Calling callback for port %d.\n", p.port);
                _callbacks[p.port](&p); // call registered callback
            }
        }
    }
}


/**
 * Packet callback surrogate.  
 */
static int nop_function() {
    return ENETDOWN;
}