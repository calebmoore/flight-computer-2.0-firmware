/*
 * worker.c
 *
 * Created: 2/1/2015 1:37:42 PM
 *  Author: Caleb
 */ 

#include <asf.h>
#include <errno.h>

#include "worker.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#define WORKER_QUEUE_LENGTH 5

struct worker_job {
	void (*function)(void *);
	void *arg;
};

static xQueueHandle worker_queue;

void worker_init() {
	if (worker_queue) {
		return;
	}
	
	worker_queue = xQueueCreate(WORKER_QUEUE_LENGTH, sizeof(struct worker_job));
}

bool worker_test() {
	return (worker_queue != NULL);
}

void worker_loop() {
	struct worker_job job;
	
	if (!worker_queue)
		return;
		
	while (1) {
		xQueueReceive(worker_queue, &job, portMAX_DELAY);
		
		if (job.function) {
			job.function(job.arg);
		}
	}
}

int worker_schedule_job(void (*function)(void *), void *arg) {
	struct worker_job job;
	
	if (!function) {
		return ENOEXEC;
	}
	
	job.function = function;
	job.arg = arg;
	
	if (xQueueSend(worker_queue, &job, 0) == pdFALSE) {
		return ENOMEM;
	}

	return 0;
}

int worker_schedule_job_from_isr(void (*function)(void *), void *arg) {
    struct worker_job job;
    BaseType_t *pxHigherPriorityTaskWoken = pdFALSE;
    int err = 0;
    
    if (!function) {
        return ENOEXEC;
    }
    
    job.function = function;
    job.arg = arg;
    
    if (xQueueSendToBackFromISR(worker_queue, &job, pxHigherPriorityTaskWoken) == pdFALSE) {
        err = ENOMEM;
    }
    
    if (*pxHigherPriorityTaskWoken == pdTRUE) {
        portYIELD_FROM_ISR(pxHigherPriorityTaskWoken);
    }
    
    return err;
}