/*
 * nmea.c
 *
 * Created: 2/13/2015 5:21:40 PM
 *  Author: Caleb
 */ 

#include <asf.h>
#include <math.h>

#include "log.h"
#include "nmeaparse.h"

#define COMBINE(sentence_type, term_number) (((unsigned int)(sentence_type) << 5) | term_number)

#define NMEA_GPRMC_TOKEN "$GPRMC"
#define NMEA_GPGGA_TOKEN "$GPGGA"
#define NMEA_SOUTH_TOKEN 'S'
#define NMEA_WEST_TOKEN  'W'
#define NMEA_DATA_INVALID_TOKEN 'V'

#define NMEA_GPGGA_LAT_OFFSET 2
#define NMEA_GPGGA_LON_OFFSET 4

#define NMEA_GPRMC_LAT_OFFSET 3
#define NMEA_GPRMC_LON_OFFSET 5

#define KNOTS_IN_M_S          0.5144f

enum sentence_type {
	GPS_SENTENCE_TYPE_GPGGA,
	GPS_SENTENCE_TYPE_GPRMC,
	GPS_SENTENCE_TYPE_OTHER
};

static bool _is_init;

static bool _flag_read;
static bool _flag_data_ready;
static char _temp_words[20][15];
static char _temp_checksum[15];

// checksum
static bool _flag_computed_chk;
static int _checksum;
static bool _flag_received_chk;
static int _index_received_chk;

// word cutting
static int _word_index;
static int _prev_index;
static int _now_index;

// stored parser results
static float _longitude;
static float _latitude;
static unsigned char _utc_hour;
static unsigned char _utc_min;
static unsigned char _utc_sec;
static unsigned char _utc_day;
static unsigned char _utc_month;
static unsigned char _utc_year;
static int _hdop;
static int _n_satellites;
static float _altitude;
static float _speed;
static float _bearing;

// stats
static uint32_t _encoded_characters;
static uint32_t _good_sentences;
static uint32_t _failed_chksum;
static uint8_t _passed_chksum;

// private functions
static void parse_data(void);
static void parse_gpgga(void);
static void parse_gprmc(void);
static void parse_utc_time(void);
static void parse_lat_lon(int lat_offset, int lon_offset);
static int int_from_hex(char a);
static float float_from_str(char *s);
static int mstrcmp(const char *s1, const char *s2);

/**
 *
 */
void nmea_init() {
	_flag_read = false;
	_flag_data_ready = false;
	
	_is_init = true;
}

/**
 *  
 */
bool nmea_test() {
	return _is_init;
}

/**
 * Picks out NMEA words and checksums from bytestream without buffer.
 *
 * TODO: refactor and split into util functions. 
 */
int nmea_fuse(char c) {
	_encoded_characters++;
		
	if (c == '$') {
		_flag_read = true;
		
		// init parser variables
		_flag_computed_chk = false;
		_checksum = 0;
		_flag_received_chk = false;
		_index_received_chk = 0;
		_word_index = _prev_index = _now_index = 0;
	}
	
	if (_flag_read) {
		// check ending
		if (c == '\r' || c == '\n') {
			_temp_words[_word_index++][_now_index - _prev_index] = 0;
						
			// cut checksum
			_temp_checksum[_index_received_chk] = 0;
			
			// sentence complete
			_flag_read = false;
			
			_good_sentences++;
			
			parse_data();
		} else {
			// computed checksum logic
			if (_flag_computed_chk && c == '*') {
				_flag_computed_chk = false;
			}
		
			if (_flag_computed_chk) {
				_checksum ^= c;
			}
		
			if (c == '$') {
				_flag_computed_chk = true;
			}
		
			// received checksum
			if (_flag_received_chk) {
				_temp_checksum[_index_received_chk++] = c;
			}
		
			if (c == '*') {
				_flag_received_chk = true;
			}
		
			// build a word
			_temp_words[_word_index][_now_index - _prev_index] = c;
			if (c == ',') {
				_temp_words[_word_index++][_now_index - _prev_index] = 0;
				_prev_index = _now_index;
			} else {
				_now_index++;
			}
		}		
	}	
	
	return _word_index;
}

bool nmea_is_data_ready() {
	return _flag_data_ready;
}

/**
 * Resets the NMEA parser to its initialized state. 
 */
void nmea_reset() {
	
}

bool nmea_get_motion(struct gps_motion *motion) {
    if (!_flag_data_ready) {
        return false;
    }

    motion->speed = _speed;
    motion->course = _bearing;
    
    return true;
}

bool nmea_get_position(struct gps_position *position) {
    if (!_flag_data_ready) {
        return false;
    }

    position->latitude = _latitude;
    position->longitude = _longitude;
    position->altitude = _altitude;
    
    return true;
}

bool nmea_get_statistics(struct gps_statistics *stats) {
    stats->chars = _encoded_characters;
    stats->sentences = _good_sentences;
    stats->chk_err = _failed_chksum;
    
    return true;
}

bool nmea_get_time(struct gps_time *time) {
    if (!_flag_data_ready) {
        return false;
    }
    
    time->sec = _utc_sec;
    time->min = _utc_min;
    time->hour = _utc_hour;
    time->day = _utc_day;
    time->month = _utc_month;
    time->year = _utc_year;
    
    return true;
}

bool nmea_get_lock_info(struct gps_lock_info *lock_info) {
    if (!_flag_data_ready) {
        return false;
    }

    lock_info->hdop = _hdop;
    lock_info->satellites = _n_satellites;

    return true;
}

/**
 *  
 */
static void parse_data() {
	int received_chk = 16*int_from_hex(_temp_checksum[0]) + int_from_hex(_temp_checksum[1]);
	
	// check chksum, return if invalid
	if (_checksum != received_chk) {
		_failed_chksum++;
		return;
	}
	
	_passed_chksum++;
	
	// check for GPGGA
	if (mstrcmp(_temp_words[0], NMEA_GPGGA_TOKEN) == 0) {
		parse_gpgga();
	} else if (mstrcmp(_temp_words[0], NMEA_GPRMC_TOKEN) == 0) {
		parse_gprmc();
	}
}

static void parse_gpgga() {
	// check GPS fix (0: none, 1: fix, 2: diff. fix)
	if (_temp_words[6][0] == '0') {
		// clear data
		_latitude = 0;
		_longitude = 0;
		_flag_data_ready = false;
			
		return;
	}
	
	parse_utc_time();
		
	parse_lat_lon(NMEA_GPGGA_LAT_OFFSET, NMEA_GPGGA_LON_OFFSET);
	
    _hdop = (float_from_str(_temp_words[6]));

	// satellites
	_n_satellites = (int)float_from_str(_temp_words[7]);
		
	// altitude
	_altitude = float_from_str(_temp_words[9]);
		
	// data ready
	_flag_data_ready = true;		
}

static void parse_gprmc() {
	// check data status (A: ok, V: invalid)
	if (_temp_words[2][0] == NMEA_DATA_INVALID_TOKEN) {
		// clear data
		_latitude = 0;
		_longitude = 0;
		_flag_data_ready = false;
		
		return;
	}
	
	parse_utc_time();
	
	parse_lat_lon(NMEA_GPRMC_LAT_OFFSET, NMEA_GPRMC_LON_OFFSET);
	
	// parse speed
	_speed = float_from_str(_temp_words[7]);
	_speed /= KNOTS_IN_M_S;
	
	// parse bearing
	_bearing = float_from_str(_temp_words[8]);
	
	// parse UTC date
	_utc_day = int_from_hex(_temp_words[9][0]) * 10 + int_from_hex(_temp_words[9][1]);
	_utc_month = int_from_hex(_temp_words[9][2]) * 10 + int_from_hex(_temp_words[9][3]);
	_utc_year = int_from_hex(_temp_words[9][4]) * 10 + int_from_hex(_temp_words[9][5]);
	
	// data ready
	_flag_data_ready = true; 
}

static void parse_utc_time() {
	_utc_hour = int_from_hex(_temp_words[1][0]) * 10 + int_from_hex(_temp_words[1][1]);
	_utc_min = int_from_hex(_temp_words[1][2]) * 10 + int_from_hex(_temp_words[1][3]);
	_utc_sec = int_from_hex(_temp_words[1][4]) * 10 + int_from_hex(_temp_words[1][5]);
}

static void parse_lat_lon(int lat_offset, int lon_offset) {
	float degrees, minutes;
	
	// parse location
	_latitude = float_from_str(_temp_words[lat_offset++]);
	_longitude = float_from_str(_temp_words[lon_offset++]);
		
	// get decimal format
	if (_temp_words[lat_offset][0] == NMEA_SOUTH_TOKEN) {
		_latitude *= -1.0;
	}
		
	if (_temp_words[lon_offset][0] == NMEA_WEST_TOKEN) {
		_longitude *= -1.0;
	}
	
	// format latitude	
	degrees = trunc(_latitude / 100.0f);
	minutes = _latitude - degrees * 100.0f;
	_latitude = degrees + minutes / 60.0f;
		
	// format longitude
	degrees = trunc(_longitude / 100.0f);
	minutes = _longitude - degrees * 100.0f;
	_longitude = degrees + minutes / 60.0f;
}

// TODO: handle things other than a-f, A-F
static int int_from_hex(char a) {
	if (a >= 'A') {
		return a - '7';
	} else {
		return a - '0';
	}
}

static float float_from_str(char *s) {
	long integer_part = 0;
	float decimal_part = 0.0;
	float decimal_pivot = 0.1;
	bool is_decimal = false;
	bool is_negative = false;
	
	char c;
	
	while ((c = *s++)) {
		// skip special chars
		if (c == '-') {
			is_negative = true;
			continue;
		}
		
		if (c == '+') {
			continue;
		}
		
		if (c == '.') {
			is_decimal = true;
			continue;
		}
		
		if (!is_decimal) {
			integer_part = (10 * integer_part) + (c - '0');
		} else {
			decimal_part += decimal_pivot * (float)(c - '0');
			decimal_pivot /= 10.0;
		}
	}
	
	decimal_part += (float)integer_part;
	
	return is_negative ? -decimal_part : decimal_part;
}

static int mstrcmp(const char *s1, const char *s2) {
	while ((*s1 && *s2) && (*s1 == *s2)) {
		s1++;
		s2++;
	}
	
	return *s1 - *s2;
}


// logging GPS
// TODO: move this somewhere sensible

//// log GPS time
LOG_GROUP_START(gps_time)
LOG_ADD(LOG_UINT8, utc_hour, &_utc_hour)
LOG_ADD(LOG_UINT8, utc_min, &_utc_min)
LOG_ADD(LOG_UINT8, utc_sec, &_utc_sec)
LOG_ADD(LOG_UINT8, utc_day, &_utc_day)
LOG_ADD(LOG_UINT8, utc_month, &_utc_month)
LOG_ADD(LOG_UINT8, utc_year, &_utc_year)
LOG_GROUP_STOP(gps_time)

// log GPS location
LOG_GROUP_START(gps_pos)
LOG_ADD(LOG_FLOAT, lat, &_latitude)
LOG_ADD(LOG_FLOAT, lon, &_longitude)
LOG_ADD(LOG_FLOAT, alt, &_altitude)
LOG_GROUP_STOP(gps_pos)

// log GPS link data
LOG_GROUP_START(gps_link)
LOG_ADD(LOG_UINT8, hdop, (uint8_t *)&_hdop)
LOG_ADD(LOG_UINT8, sats, (uint8_t *)&_n_satellites)
LOG_GROUP_STOP(gps_link)

// log GPS motion data
LOG_GROUP_START(gps_motion)
LOG_ADD(LOG_FLOAT, speed, &_speed)
LOG_ADD(LOG_FLOAT, course, &_bearing)
LOG_GROUP_STOP(gps_motion)

// log GPS stats
LOG_GROUP_START(gps_stats)
LOG_ADD(LOG_UINT8, data_good, &_flag_data_ready)
LOG_ADD(LOG_UINT32, chars, &_encoded_characters)
LOG_ADD(LOG_UINT32, sent, &_good_sentences)
LOG_ADD(LOG_UINT32, chkfail, &_failed_chksum)
LOG_GROUP_STOP(gps_stats)