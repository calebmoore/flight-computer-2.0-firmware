/*
 * eprintf.c
 *
 * Created: 2/4/2015 7:00:52 PM
 *  Author: Winglee Plasma Lab
 */ 

#include "eprintf.h"

#include <stdarg.h>
#include <stdint.h>
#include <ctype.h>

static const char digit[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        'A', 'B', 'C', 'D', 'E', 'F'};
        
static int get_int_len(int value) {
    int l = 1;
    
    while (value > 9) {
        l++;
        value /= 10;
    }
    
    return l;
}

static int power(int a, int b) {
    int i;
    int x = a;
    
    for (i = 1; i < b; i++) {
        x *= a;
    }
    
    return x;
}

static int itoa(putc_func putcf, int num, int base, int precision) {
    long int i = 1;
    int len = 0;
    unsigned int n = num;
    int num_length = get_int_len(num);
    int fill_with_zero = 0;
    
    if (num == 0) {
        putcf('0');
        return 1;
    }
    
    if (num < 0 && base == 10) {
        n = -num;
        putcf('-');
    }
    
    if (num_length < precision) {
        fill_with_zero = precision - num_length;
        while (fill_with_zero > 0) {
            putcf('0');
            len++;
            fill_with_zero--;
        }
    }
    
    while (n / i) {
        i *= base;
    }
    
    while (i /= base) {
        putcf(digit[(n / i) % base]);
        len++;
    }
    
    return len;
}

int evprintf(putc_func putcf, const char *fmt, va_list ap) {
    int len = 0;
    double num;
    uint32_t *l;
    char *str;
    int precision;
    
    while (*fmt) {
        precision = 6;
        
        if (*fmt == '%') {
            while (!isalpha((unsigned) *++fmt)) {
                if (*fmt == '.') {
                    if (isdigit((unsigned) *++fmt)) {
                        precision = *fmt - '0';
                    }
                }
            }
            
            switch (*fmt++) {
                case 'i':
                case 'd':
                    len += itoa(putcf, va_arg(ap, int), 10, 0);
                    break;
                case 'x':
                case 'X':
                    len += itoa(putcf, va_arg(ap, int), 16, 0);
                    break;
                case 'f':
                    // When pulling 64-bit values from va_arg, arm-gcc exhibits
                    // unexpected behavior when ap is not placed on an 8 byte
                    // boundary.
                    // This is a hack to get a double from the list.
                    l = (uint32_t *)&num;
                    if (*(uint32_t *)&ap & 4) {
                        l[0] = va_arg(ap, uint32_t);
                        l[1] = va_arg(ap, uint32_t);
                    } else {
                        l[0] = va_arg(ap, uint32_t);
                        l[0] = va_arg(ap, uint32_t);
                        l[1] = va_arg(ap, uint32_t);
                    }
                    
                    if (num < 0) {
                        putcf('-');
                        num = -num;
                        len++;
                    }
                    
                    len += itoa(putcf, (int)num, 10, 0);
                    putcf('.');
                    len++;
                    len += itoa(putcf, (num - (int)num) * power(10, precision), 10, precision);
                    break;
                case 's':
                    str = va_arg(ap, char *);
                    while (*str) {
                        putcf(*str++);
                        len++;
                    }
                    break;
                default:
                    break;
            }
        } else {
            putcf(*fmt++);
            len++;
        }
    }
    
    return len;
}

int eprintf(putc_func putcf, const char *fmt, ...) {
    va_list ap;
    int len;
    
    va_start(ap, fmt);
    len = evprintf(putcf, fmt, ap);
    va_end(ap);
    
    return len;
}