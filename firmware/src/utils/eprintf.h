/*
 * eprintf.h
 *
 * Created: 2/4/2015 7:00:43 PM
 *  Author: Winglee Plasma Lab
 */ 


#ifndef EPRINTF_H_
#define EPRINTF_H_

#include <stdarg.h>

/**
 * putc function pointer
 */
typedef int (*putc_func)(int c);

int eprintf(putc_func putcf, const char *fmt, ...) 
        __attribute__((format(printf, 2, 3)));

int evprintf(putc_func putcf, const char *fmt, va_list ap);


#endif /* EPRINTF_H_ */