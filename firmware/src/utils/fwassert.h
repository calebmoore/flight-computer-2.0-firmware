/*
 * fwassert.h
 *
 * Created: 2/4/2015 6:08:06 PM
 *  Author: Winglee Plasma Lab
 */ 


#ifndef FWASSERT_H_
#define FWASSERT_H_

#define ASSERT(e) if (e) ; \
        else assertFail(#e, __FILE__, __LINE__)

void assertFail(const char *exp, const char *file, int line);

#endif /* FWASSERT_H_ */