/*
 * fwassert.c
 *
 * Created: 2/4/2015 6:09:30 PM
 *  Author: Winglee Plasma Lab
 */ 

#include "fwassert.h"
#include "statind.h"

/**
 * If something goes wrong, halt operation and alert user.
 */
void assertFail(const char *exp, const char *file, int line) {
    statind_clear_all();
    statind_set(LED_STATUS_1, true);
    statind_set(LED_STATUS_2, true);
    statind_set(BUZZER, true);
    
    while (1);
}