/*
 * debug.h
 *
 * Created: 2/18/2015 7:01:21 PM
 *  Author: Caleb
 */ 

#include <asf.h>

#ifndef DEBUG_H_
#define DEBUG_H_

#define DEBUG_OVER_USART

#ifdef DEBUG_OVER_USART
#	define DEBUG_PRINT(fmt, ...) eprintf(uart_putchar, fmt, ##__VA_ARGS__)
#else
#	define DEBUG_PRINT(fmt, ...) 
#endif

#endif /* DEBUG_H_ */