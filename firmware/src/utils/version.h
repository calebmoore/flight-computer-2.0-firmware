/*
 * version.h
 *
 * Created: 2/18/2015 8:19:55 PM
 *  Author: Caleb
 */ 


#ifndef VERSION_H_
#define VERSION_H_

#define V_MAJOR "0"
#define V_MINOR "0"
#define V_REV   "0"
#define V_BUILD "1"
#define V_MODIFIED false

#endif /* VERSION_H_ */